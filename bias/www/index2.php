<?php $geoloc = $framework->get("geoloc");
$lang = $geoloc->language;
//$framework->set("topnav", "idfa");
?>
<!DOCTYPE HTML>
<html><head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        <meta http-equiv="x-ua-compatible" content="IE=9">
        <script>
		var lang = "<?php echo $lang?>";
		</script>
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.2/TweenMax.min.js"></script>
      	<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.17.0/utils/Draggable.min.js"></script>
      	<script src="greensock-js/minified/plugins/ThrowPropsPlugin.min.js"></script>
       	<script src="js/main.js?nocache=<?php echo rand()?>"></script>
       	<script src="js/questions_<?php echo $lang?>.js"></script>
       	
       	<script src="js/pictos.js"></script>
        <title>Bias</title>
		<link rel="stylesheet" type="text/css" href="css/styles.css?nocache=<?php echo rand()?>">
		
 		
  		<?php $framework->show_dependencies(); ?>
      	<?php $framework->show_share_meta(); ?>
    </head>
    <body>
	
         <?php $framework->show_header(); ?>
<div id="container">

  <div class="swipe blue" id="swipe_1"><div class="statement"><div class="number"></div><div class="separator"><img src="images/spacer.gif"></div><div class="text"></div></div></div>
			<div class="swipe" id="swipe_0"><div class="statement"><div class="number"></div><div class="separator"></div><div class="separator"><img src="images/spacer.gif"></div><div class="text"></div></div></div>
			<div class="swipe black" id="swipe_2"><div class="statement_indications"><div class="indications"></div></div><div class="okbutton"><a href="#"><img src="images/btn_ok.png" width="54" height="54" alt="" border="0" onclick="closeInstructions1();"/></a></div><div class="nextbutton"><a href="#"><img src="images/btn_next.png" width="54" height="54" alt="" border="0" onclick="nextIntro();"/></a></div></div>
			<div class="swipe black" id="swipe_3"><div class="statement_indications"><div class="indications"></div></div><div class="okbutton"><a href="#"><img src="images/btn_ok.png" width="54" height="54" alt="" border="0" onclick="closeInstructions2();"/></a></div></div>
			
			<div class="swipe black" id="swipe_4"><div class="statement_indications"><div class="indications"><br><br><br>According to your answers, you are *******.<br><br>It takes more time to... than to... <br><br><span style="font-size:15px"><u>How does this work?</u></span></div></div><div class="okbutton"><a href="#"><img src="images/btn_next.png" width="54" height="54" alt="" border="0" onclick="closeConclusion1();"/></a></div></div>
			<div class="tiny"><img src="images/tiny_bias.png" width="9" height="36" alt=""/></div>
			<div class="logo"><div class="logoimage"><img src="images/logo_<?php echo $lang?>.png" width="200" height="90" alt="" class="logogif"/></div></div>
			
			<div class="introtext"><!--Lorem ipsum dolor sit amet...--></div>

		</div>
		
		
		<!-- pictos -->
		<div id="icon_racism">
			<div id="icon_racism_circle_white"><img src="images/icon_racism_circle.png" width="188" height="188" alt=""/></div>		
			<div id="icon_racism_circle_black"><img src="images/icon_circle_black.png" width="188" height="188" alt=""/></div>
			<div id="icon_racism_circle_empty"><img src="images/icon_circle_empty.png" width="188" height="188" alt=""/></div>
					
		</div>

		<div id="icon_sexism">
			<div id="icon_sexism_circle_empty_1">
				<div class="female"><img src="images/icn_female.png" width="41" height="61" alt=""/></div>
				<div class="img"><img src="images/icon_circle_black.png" width="188" height="188" alt=""/></div>
			</div>
			<div id="icon_sexism_circle_empty_2">
				<div class="male"><img src="images/icn_male.png" width="51" height="51" alt=""/></div>
				<div class="img"><img src="images/icon_circle_black.png" width="188" height="188" alt=""/></div>
			</div>			
		</div>

		<div id="icon_homophobia">

		  <div id="icon_homophobia_circle_empty_2">
				<div class="male2"><img src="images/icn_male.png" width="51" height="51" alt=""/></div>
				<div class="img"><img src="images/icon_circle_lines.png" width="188" height="188" alt=""/></div>
			</div>
			<div id="icon_homophobia_circle_empty_1">
				<div class="male1"><img src="images/icn_male.png" width="51" height="51" alt=""/></div>
				<div class="img"><img src="images/icon_circle_black.png" width="188" height="188" alt=""/></div>
			</div>			
		</div>

		<div id="icon_ageism">
			<div id="icon_ageism_circle_white"><img src="images/icon_racism_circle.png" width="188" height="188" alt=""/></div>		
			<div id="icon_ageism_circle_black"><div><img src="images/icon_circle_black.png" width="188" height="188" alt=""/></div><div id="icon_ageism_circle"><img src="images/icon_ageism_circle.png" width="26" height="26" alt=""/></div>
			<div id="icon_ageism_hour"><img src="images/icon_ageism_hour.png" width="56" height="28" alt=""/></div>
		  <div id="icon_ageism_arrow"><img src="images/icon_ageism_arrow.png" width="83" height="28" alt=""/></div></div>

		</div>

		<div id="icon_religion">
			<div id="icon_religion_halo"><img src="images/icon_religion_halo.png" width="188" height="40" alt=""/></div>	
			<div id="icon_religion_circle_black"><div><img src="images/icon_circle_black.png" width="188" height="188" alt=""/></div><div id="icon_religion_cross"><img src="images/icon_religion_cross.png" width="61" height="116" alt=""/></div>		
			</div>		
		</div>

		<div id="icon_weight">
			<div id="icon_weight_scale"><img src="images/icon_weight_scale.png" width="287" height="176" alt=""/></div>	 		
			<div id="icon_weight_circle_black"><div><img src="images/icon_circle_black.png" width="188" height="188" alt=""/></div><div id="icon_weight_circle"><img src="images/icon_ageism_circle.png" width="26" height="26" alt=""/></div>
			<div id="icon_weight_hour"><img src="images/icon_ageism_hour.png" width="56" height="28" alt=""/></div></div>		
		</div>

		<div id="icon_handicap">	
			<div id="icon_handicap_circle"><div id="icon_handicap_bar1"><img src="images/icon_ageism_hour.png" width="97" height="28" alt=""/></div>
			<div id="icon_handicap_bar2"><img src="images/icon_ageism_hour.png" width="56" height="28" alt=""/></div><div><img src="images/icon_circle_black.png" width="188" height="188" alt=""/></div></div>	
			<div id="icon_handicap_wheel"><img src="images/icn_handicap_wheel.png" width="188" height="188" alt=""/></div>	
		</div>
		
		<div id="icon_weapons">
			<div id="icon_weapons_circle"><img src="images/icon_weapons.png" width="188" height="188" alt=""/></div>		
			<div id="icon_weapons_trigger"><img src="images/icn_weapon_trigger.png" width="64" height="46" alt=""/></div>
					
		</div>
		
		<div class="type">African Americans</div>
		<div id="contour"></div>	
		<div id="fail"><img src=images/icn_fail_white.png width='82' height='83'/></div>
		<div id='okslide'><a href='#'><img src='images/btn_ok.png' width='54' height='54' border='0' onclick='gotIt();'/></a></div>
   
   <label for="slider-1">Input slider:</label>
<input type="range" name="slider-1" id="slider-1" value="0" min="-50" max="50" step="5">
<div id="slider_text">
	<div id="slider_not">Not at all</div>
	<div id="slider_yes">Very much</div>
</div>
 
  <div id="drop"><img src="images/drop2.png" width="55" height="55" alt=""/></div>
  <div id="slider_guess">Your guess</div>
<div id="slider_result">Reality</div>
  <div id="swipe_left">woman — positive</div>
  <div id="swipe_right">woman — positive</div>
   <!--<?php $framework->show_footer(); ?>-->
        <?php $framework->show_tagging_tools(); ?>
        <script>   
          var fW = new Framework('<?php echo $framework->exportToJS(); ?>');
          // fW.track("test_de_statistique"); //exemple de tag
        </script>
       
  <div id="how">
 
  <div id="how_content_holder">
  <?php if($lang == "en") {?>
  <div id="how_content">In an era in which we are constantly making snap decisions and judgments on our phones – whether liking a tweet or image on social media or choosing the news we consume, the circles of people we associate with and date, or the products we buy – an underlying bias is always present. Influenced by a lifetime of cultural references, we're continually shaping and narrowing the filtered lens through which we experience the world, through millions of data points collected by all the apps whose terms and conditions we never read. Despite our assumptions that we're generally open-minded, we are often unconsciously making stereotypical associations regarding those we see as "other", whether that's of another race, gender, sex, religion, size, ability, or age. Bias is inspired by Harvard's <a href="https://implicit.harvard.edu/implicit/">Project Implicit</a>, an ongoing study that aims to educate the public about hidden biases and to provide a virtual laboratory for collecting data on the Internet. We adapted the basic principles of their implicit association tests to a quicker mobile format to reach an audience accustomed to making quick judgment calls on the go.<br><br>In this experience, we measure the number of seconds it takes for you to make positive or negative associations with certain groups of people. The more time it takes you to associate positive words with a certain group (e.g. "amazing" with "lesbian"), the more biased you are against them. While this experience may not be as exhaustive as Harvard's, we hope it inspires you to consider your biases in a different light.<br><br><br><br><br><br><br></div></div>
  <?php } else if($lang == "fr") {?>
  <div id="how_content">En cette époque où nous sommes constamment appelés à prendre des décisions et porter des jugements rapides sur notre téléphone portable – qu'il s'agisse d' « aimer » un tweet ou une image sur les réseaux sociaux, de choisir nos sources d'information, les gens avec qui nous nous associons et que nous fréquentons ou même les produits que nous achetons – un préjugé sous-jacent est toujours présent. Influencée par une vie entière de références culturelles, au fil des points de données recueillies par toutes ces applications dont on ne lit jamais les conditions d'utilisation, la lentille à travers laquelle nous percevons le monde se transforme et rétrécit sans cesse. En dépit de notre impression d'être relativement ouverts d'esprit, nous faisons souvent des associations stéréotypées au sujet de ceux que nous percevons comme « les autres », qu'ils diffèrent de nous en raison de leur race, leur genre, leur sexe, leur religion, leur grosseur, leurs capacités physiques ou leur âge. Préjugés est inspiré de l'initiative <a href="https://implicit.harvard.edu/implicit/">Project Implicit</a>, une étude en cours de l'université de Harvard, qui vise à éduquer le public sur les préjugés cachés tout en offrant un laboratoire virtuel permettant de recueillir des données sur Internet. Nous avons adapté les principes de base de leurs tests d'association implicite à un format mobile plus rapide, destiné à rejoindre un auditoire qui a l'habitude de porter des jugements rapides du bout des doigts. <br><br>Au cours de cette expérience, nous mesurons le nombre de secondes qu'il vous faut pour faire des associations positives ou négatives avec certains groupes de personnes. Plus il vous faut de temps pour associer des mots positifs à un certain groupe (ex : « fantastique » avec « lesbienne »), plus vous entretenez de préjugés à leur égard. Bien que notre expérience soit moins exhaustive que celle de Harvard, nous espérons qu'elle vous inspirera à reconsidérer vos préjugés en les montrant sous un nouveau jour.<br><br><br><br><br><br><br></div></div>
  <?php } else {?>
  <div id="how_content">In einer Zeit, in der wir auf unseren Handys ständig schnelle Entscheidungen treffen und Urteile fällen – wenn wir einen Tweet oder ein Bild in sozialen Netzwerken "liken", uns für bestimmte Nachrichtenquellen entscheiden, Menschen auswählen, mit denen wir uns austauschen oder treffen, Waren, die wir kaufen –, immer sind unterschwellige Vorurteile präsent. Wir werden ein Leben lang geprägt von kulturellen Bezügen, und wir gestalten und verengen unsere Sicht auf die Welt ständig, beeinflusst durch Millionen von Daten, gesammelt von Apps, deren Nutzungsbedingungen wir nie gelesen haben. Obwohl wir uns im Allgemeinen als weltoffen bezeichnen, haben wir oft unbewusst Vorurteile, wenn wir "andere" sehen, die sich von uns durch Herkunft, Geschlecht, Religion, Größe, Fähigkeiten oder Alter unterscheiden. VORURTEILE lehnt sich an an die Studie <a href="https://implicit.harvard.edu/implicit/">Project Implicit</a> der Harvard-Universität, mit der die Öffentlichkeit über unbewusste Vorurteile aufgeklärt und ein virtuelles Labor für die Sammlung entsprechender Daten geschaffen werden soll. Wir haben die Grundzüge ihres "Impliziten Assoziationstests" an ein schnelleres Handy-Format angepasst, um Probanden zu erreichen, die es gewohnt sind, schnelle Entscheidungen zu fällen.<br><br>Bei diesem Experiment messen wir die Zahl der Sekunden, die Sie benötigen, um positive oder negative Assoziationen mit bestimmten Gruppen von Menschen zu verbinden. Je länger Sie brauchen, um positive Wörter für bestimmte Gruppen zu finden (z. B. "toll" für "lesbisch"), umso größer ist Ihr Vorurteil gegenüber dieser Gruppe. Das Experiment ist natürlich nicht so ausführlich wie das der Harvard-Universität, aber wir hoffen, dass Sie dadurch Ihre Vorurteile in einem neuen Licht sehen.<br><br><br><br><br><br><br></div></div>
  <?php }?>
   <div id="how_header">
  	<?php if($lang == "en") {?><br>Find out how <br>this works<br>
  	<?php } else if($lang == "fr") {?><br>Comment<br>ça marche<br>
  	<?php } else {?><br>wie<br>funktioniert es<br>
  	<?php }?>
  	<img src="images/white2.png" width="2" height="40" alt=""/> </div>
  	<div id="how_close"><a href="#" onclick="closeHow()"><img src="images/btn_close.png" width="21" height="21" alt="" border="0"/></a></div>
		</div>
		<div id="credits">
 
  <div id="credits_content_holder">
  <div id="credits_content">
La vie, c’est court. Maintenant plus que jamais. On veut tout. Tout de suite. Et maintenant. Très très court  est une collection de 10 projets interactifs qui explorent le thème de la mobilité en 60 secondes. Une production de l’ONF et ARTE, en partenariat avec IDFADocLab.
<br><br>
Life is short. Now more than ever. We want it all. We want it now. Right away. Very very short is a collection of 10 interactive projects for smartphones, exploring the theme of mobility through very, very short experiences – all under 60 seconds. A NFB and ARTE coproduction, in collaboration with IDFA DocLab.
<br><br>
Das Leben ist kurz. Mehr als je zuvor. Wir wollen alles. Und wir wollen es jetzt. Sehr Sehr Kurz ist eine Serie aus 10 interaktiven Projekten, bei denen das Thema Mobilität jeweils in 60 Sekunden behandelt wird. Eine Koproduktion von National Film Board of Canada und ARTE, in Zusammenarbeit mit IDFADocLab.<br><br>
  ONF / NFB<br><br>Executive Producer / Producteur exécutif / Produktionsausführung<br>Hugues Sweeney<br><br>Production Managers  / Chargées de production / Produktionsmanagement<br>Marie-Pier Gauthier<br>Nathalie Bédard Morin<br><br>Editorial Manager  / Chargée d’édition / Redaktionsleitung<br>Valérie Darveau<br><br>Technology director  /  Directeur des technologies / Technische Leitung<br>Martin Viau<br><br>Production Coordinators  / Coordonnatrices de production / Koordination<br>Caroline Fournier<br>Claudia Boutin<br>Dominique Brunet<br><br>Administrator / Administratrice / Verwaltung<br>Marie-Andrée Bonneau<br><br>Marketing Manager / Agente de mise en marché / Management Marketing<br>Tammy Peddle<br><br>Marketing coordinator / Coordonnateur marketing / Koordination Marketing<br>Florent Prevelle<br> <br>Stratège médias sociaux / Social Media Strategist / Strategie Soziale Medien<br>Kate Ruscito <br><br>Chargé de projet contenu web / Web content Project manager / Management Web-Content<br>Félix-Antoine Viens<br><br>Press Relations / Relations de presse / Pressearbeit<br>Marie-Claude Lamoureux<br>Pat Dillon<br><br>Technologies de l’information / Information technologies / IT<br>Sergiu Suciu<br> <br>Services juridiques / Legal Services / Rechtsberatung<br>Peter Kallianiotis<br><br><br>ARTE France <br>Directeur du développement numérique  / Digital Development Chief / Leitung Pôle Web<br>Gilles Freissinier<br><br>Responsable éditorial  / Editorial Manager / Leitung Webproduktionen<br>Marianne Levy-Leblond<br><br>Chargées de programmes  / Program Producers / Programmverantwortliche<br>Marie Berthoumieu<br>Emilie Bessard  <br><br>Responsable Pôle Projet  / Head of project management / Projektmanagement<br>Stéphane Nauroy<br> <br>Chargée des réseaux sociaux / Community manager / Leitung Soziale Medien<br>Léa Jagut<br><br>Administrateur  / Administrator / Administrator<br>Jerôme Vernet<br> <br>ARTE France Développement<br>Directrices de Production  / Production Managers / Produktionsleitung<br>Claire Aubret<br>Angèle Le Névé <br>Assistées de  / Assisted by / Assistenz<br>Juliette Droillard<br>Traduction  / Translation / Übersetzung<br>(…), (…), Christian Stonner<br> <br>Responsable ARTE Studio Lab / ARTE Studio Lab Responsibles / Verantwortlicher ARTE Studio Lab<br>Clément Dhamelincourt<br>Responsable technique  / Technical Manager / Technischer Leiter<br>Vincent Chagny<br>Suivi de projet / Project Manager / Projektmanager<br>Noé Daudin-Clavaud<br><br><br>CRÉDITS IDFA DOCLAB / IDFA DOCLAB CREDITS / MITWIRKENDE IDFA DOCLAB<br><br>Appel à projets / Call for projects / Ausschreibung<br>Caspar Sonnen<br>Wotienke Vermeer<br>Michael Zbieranowski<br><br>REMERCIEMENTS | ACKNOWLEDGEMENTS / DANK<br><br>Caspar Sonnen - IDFA DocLab<br>Ziv Schneider - Artiste nouveaux médias / New media artist / Künstlerin Neue Medien<br>Jouke Vuurmans - MediaMonks<br>Julia Kaganskiy - New Museum<br>Myriam Achard - PHI Center<br>Florent Maurin - The Pixel Hunt<br>Jepchumba - Artiste numérique et curateur  / Digital Artist and Curator / Digitaler Künstler und Kurator<br>Marie Berthoumieu - ARTE<br>Hugues Sweeney - ONF / NFB / National Film Board of Canada<br><br>Rafaella Wang<br>Zina Kisch<br><br>© ARTE  FRANCE - ONF 2017<br><br><br><br></div></div>
   <div id="credits_header">
  	<?php if($lang == "en") {?><br>About the<br>project<br><?php } else if($lang == "fr"){ ?><br>À<br>propos<br><?php } else if($lang == "de") {?><br>Über das<br>Projekt<br><?php }?>
  	<img src="images/white2.png" width="2" height="40" alt=""/> </div>
  	<div id="credits_close"><a href="#" onclick="closeCredits()"><img src="images/btn_close.png" width="21" height="21" alt="" border="0"/></a></div></div>
   </body>
</html>
