<?php
return array(
	"projectName" => "Bias",
	"external" => false,
	"root_folder" => "/bias/",
	"www_folder" => "www",
	"cdn" => "https://amazon.ca",
	"share" => [ 
		"en" => [
			"title" => "My Title EN",
			"description" => "My Description EN",
			"tweet" => "tweet description avec #hashtag and @onf EN",
			"image" => "img/default.png",
			"video_url" => "Video URL EN",
			"video_secure_url" => "video secure URL EN",
			"video_type" => "text/html",
			"video_width" => "200",
			"video_height" => "100"
		],
		"fr" => [
			"title" => "My Title FR",
			"description" => "My Description FR",
			"tweet" => "tweet description avec #hashtag and @onf Fr",
			"image" => "img/default.png",
			"video_url" => "Video URL FR",
			"video_secure_url" => "video secure URL FR",
			"video_type" => "text/html",
			"video_width" => "200",
			"video_height" => "100"
		],
		"de" => [
			"title" => "My Title DE",
			"description" => "My Description DE",
			"tweet" => "tweet description avec #hashtag and @onf DE",
			"image" => "img/default.png",
			"video_url" => "Video URL DE",
			"video_secure_url" => "video secure URL DE",
			"video_type" => "text/html",
			"video_width" => "200",
			"video_height" => "100"
		]
	]
);
?>