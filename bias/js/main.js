var aColors = [];
var dragStartPos = 0;
var origx = 0;
var origy = 0;
var globalStatementId = 0;
var currentParentId = 0;
var currentId = 0;
var currentSwipe = 0;
var otherSwipe = 1;

var screenWidth = 0;
var statementShowTime =0;
var statementAnswerTime = 0;
var draggers = new Array();
var d = new Date();
var aAnswers = [];
var currentSession =-1;

var aStatementBack = [];
var aStatements = [];

var flip = false;

if(lang == "en") {
	var aCategories = []
	//<img src='images/white.png' width='1' height='28'><br><br>
	aCategories.push("Swipe left for words or images that are <u>unpleasant</u> or associated with <u>black celebrities</u>.<br><br>Swipe right for words or images that are <u>pleasant</u> or associated with <u>white celebrities.<br><br><u>Go as fast as you can.</u>")
	aCategories.push("Swipe left for words or images that are associated with <u>women or family</u>.<br><br>Swipe right for words or images that are associated with <u>men or work</u>.<br><br><u>Go as fast as you can.</u>")
	aCategories.push("Swipe left for words or images that are <u>unpleasant</u> or related to <u>homosexuals</u>.<br><br>Swipe right for words or images that are <u>pleasant</u> or related to <u>heterosexuals</u>.<br><br><u>Go as fast as you can.</u>")
	aCategories.push("Swipe left for words or images that are <u>unpleasant</u> or related to <u>the elderly</u>.<br><br>Swipe right for words or images that are <u>pleasant</u> or related to <u>young people</u>.")
	aCategories.push("Swipe left for words or images that are <u>unpleasant</u> or related to <u>Islam</u>.<br><br>Swipe right for words or images that are <u>pleasant</u> or related to <u>Christianity</u>.<br><br><u>Go as fast as you can.</u>")
	aCategories.push("Swipe left for words or images that are <u>unpleasant</u> or related to <u>fat people</u>.<br><br>Swipe right for words or images that are <u>pleasant</u> or related to <u>thin people</u>.<br><br><u>Go as fast as you can.</u>")
	aCategories.push("Swipe left for words or images that are <u>unpleasant</u> or related to <u>disabled people</u>.<br><br>Swipe right for words or images that are <u>pleasant</u> or related to <u>able-bodied people</u>.<br><br><u>Go as fast as you can.</u>");


	var aCategories2 = []

	aCategories2.push("Swipe left for words or images that are <u>pleasant</u> or associated with <u>black celebrities</u>.<br><br>Swipe right for words or images that are <u>unpleasant</u> or associated with <u>white celebrities</u>.<br><br><u>Go as fast as you can.</u>")
	aCategories2.push("Swipe left for words or images that are associated with <u>women or work</u>.<br><br>Swipe right for words or images that are associated with <u>men or family</u>.<br><br><u>Go as fast as you can.</u>")
	aCategories2.push("Swipe left for words or images that are <u>pleasant</u> or related to <u>homosexuals</u>.<br><br>Swipe right for words or images that are <u>unpleasant</u> or related to <u>heterosexuals</u>.<br><br><u>Go as fast as you can.</u>")
	aCategories2.push("Swipe left for words or images that are <u>pleasant</u> or related to <u>the elderly</u>.<br><br>Swipe right for words or images that are <u>unpleasant</u> or related to <u>young people</u>.<br><br><u>Go as fast as you can.</u>")
	aCategories2.push("Swipe left for words or images that are <u>pleasant</u> or related to <u>Islam</u>.<br><br>Swipe right for words or images that are <u>unpleasant</u> or related to <u>Christianity</u>.<br><br><u>Go as fast as you can.</u>")
	aCategories2.push("Swipe left for words or images that are <u>pleasant</u> or related to <u>fat peoplea</u>.<br><br>Swipe right for words or images that are <u>unpleasant</u> or related to <u>thin people</u>.<br><br><u>Go as fast as you can.</u>")
	aCategories2.push("Swipe left for words or images that are <u>pleasant</u> or related to <u>disabled people</u>.<br><br>Swipe right for words or images that are <u>unpleasant</u> or related to <u>able-bodied people</u>.<br><br><u>Go as fast as you can.</u>");
} else if(lang == "fr") {
	
	setTimeout( function() {
	$("#slider_not").text("Pas du tout")
	$("#slider_yes").text("Énormément")
	$("#slider_guess").text("Votre choix")
	$("#slider_result").text("La réalité")
	
	}, 1500);
	var aCategories = []
	
	aCategories.push("Faites glisser vers la gauche les images ou les mots <u>désagréables</u> ou associés aux <u>célébrités noires</u>.<br><br>Faites glisser vers la droite les images ou les mots <u>agréables</u> ou associés aux <u>célébrités blanches.<br><br><u>Allez aussi vite que possible.</u>")
	aCategories.push("Faites glisser vers la gauche les images ou les mots associés aux <u>femmes ou la famille</u>.<br><br>Faites glisser vers la droite les images ou les mots associés aux <u>hommes ou le travail</u>.<br><br><u>Allez aussi vite que possible.</u>")
	aCategories.push("Faites glisser vers la gauche les images ou les mots <u>désagréables</u> ou associés aux <u>homosexuels</u>.<br><br>Faites glisser vers la droite les images ou les mots <u>agréables</u> ou associés aux <u>hétérosexuels</u>.<br><br><u>Allez aussi vite que possible.</u>")
	aCategories.push("Faites glisser vers la gauche les images ou les mots <u>désagréables</u> ou associés aux <u>vieux</u>.<br><br>Faites glisser vers la droite les images ou les mots <u>agréables</u> ou associés aux <u>jeunes</u>.<br><br><u>Allez aussi vite que possible.</u>")
	aCategories.push("Faites glisser vers la gauche les images ou les mots <u>désagréables</u> ou associés à l'<u>Islam</u>.<br><br>Faites glisser vers la droite les images ou les mots <u>agréables</u> ou associés au <u>Christianisme</u>.<br><br><u>Allez aussi vite que possible.</u>")
	aCategories.push("Faites glisser vers la gauche les images ou les mots <u>désagréables</u> ou associés aux <u>grosses personnes</u>.<br><br>Faites glisser vers la droite les images ou les mots <u>agréables</u> ou associés aux <u>personnes minces</u>.<br><br><u>Allez aussi vite que possible.</u>")
	aCategories.push("Faites glisser vers la gauche les images ou les mots <u>désagréables</u> ou associés aux <u>personnes handicapées</u>.<br><br>Faites glisser vers la droite les images ou les mots <u>agréables</u> ou associés aux <u>personnes non handicapées</u>.<br><br><u>Allez aussi vite que possible.</u>");


	var aCategories2 = []

	aCategories2.push("Faites glisser vers la gauche les images ou les mots <u>agréables</u> ou associés aux <u>célébrités noires</u>.<br><br>Faites glisser vers la droite les images ou les mots <u>désagréables</u> ou associés aux <u>célébrités blanches</u>.<br><br><u>Allez aussi vite que possible.</u>")
	aCategories2.push("Faites glisser vers la gauche les images ou les mots associés aux <u>femmes ou le travail</u>.<br><br>Faites glisser vers la droite les images ou les mots associés aux <u>hommes ou la famille</u>.<br><br><u>Allez aussi vite que possible.</u>")
	aCategories2.push("Faites glisser vers la gauche les images ou les mots <u>agréables</u> ou associés aux <u>homosexuels</u>.<br><br>Faites glisser vers la droite les images ou les mots <u>désagréables</u> ou associés aux <u>hétérosexuels</u>.<br><br><u>Allez aussi vite que possible.</u>")
	aCategories2.push("Faites glisser vers la gauche les images ou les mots <u>agréables</u> ou associés aux <u>vieux</u>.<br><br>Faites glisser vers la droite les images ou les mots <u>désagréables</u> ou associés aux <u>jeunes</u>.<br><br><u>Allez aussi vite que possible.</u>")
	aCategories2.push("Faites glisser vers la gauche les images ou les mots <u>agréables</u> ou associés à l'<u>Islam</u>.<br><br>Faites glisser vers la droite les images ou les mots <u>désagréables</u> ou associés aux <u>Christianisme</u>.<br><br><u>Allez aussi vite que possible.</u>")
	aCategories2.push("Faites glisser vers la gauche les images ou les mots <u>agréables</u> ou associés aux <u>grosses personnes</u>.<br><br>Faites glisser vers la droite les images ou les mots <u>désagréables</u> ou associés aux <u>personnes minces</u>.<br><br><u>Allez aussi vite que possible.</u>")
	aCategories2.push("Faites glisser vers la gauche les images ou les mots <u>agréables</u> ou associés aux <u>personnes handicapées</u>.<br><br>Faites glisser vers la droite les images ou les mots <u>désagréables</u> ou associés aux <u>personnes non handicapées</u>.<br><br><u>Allez aussi vite que possible.</u>");
} else if(lang == "de") {
	setTimeout( function() {
	$("#slider_not").text("Überhaupt nicht")
	$("#slider_yes").text("Sehr")
	$("#slider_guess").text("Ihre Einschätzung")
	$("#slider_result").text("Die Realität")
	
	}, 1500);
	var aCategories = []
	
	aCategories.push("Wischen Sie Begriffe, die <u>unangenehm</u> sind oder mit <u>berühmten Schwarzen</u> zu tun haben, nach links. Wischen Sie Begriffe, die <u>angenehm</u> sind oder mit <u>berühmten Weißen</u> zu tun haben, nach rechts.<br><br><u>Tun Sie das so schnell wie möglich.</u>")
	aCategories.push("Wischen Sie Bilder oder Begriffe, die Sie mit <u>Frauen oder Familie</u> verbinden, nach links. Wischen Sie Bilder oder Begriffe, die Sie mit <u>Männern oder Arbeit</u> verbinden, nach rechts.<br><br><u>Tun Sie das so schnell wie möglich.</u>")
	aCategories.push("Wischen Sie Bilder oder Begriffe, die <u>unangenehm</u> sind oder mit Homosexuellen zu tun haben, nach links. Wischen Sie Bilder oder Begriffe, die <u>angenehm</u> sind oder mit Heterosexuellen zu tun haben, nach rechts.<br><br><u>Tun Sie das so schnell wie möglich.</u>")
	aCategories.push("Wischen Sie Bilder oder Begriffe, die <u>unangenehm</u> sind oder mit <u>alten Menschen</u> zu tun haben, nach links. Wischen Sie Bilder oder Begriffe, die <u>angenehm</u> sind oder mit <u>jungen Menschen</u> zu tun haben, nach rechts.<br><br><u>Tun Sie das so schnell wie möglich.</u>")
	aCategories.push("Wischen Sie Bilder oder Begriffe, die <u>unangenehm</u> sind oder mit dem <u>Islam</u> zu tun haben, nach links. Wischen Sie Bilder oder Begriffe, die <u>angenehm</u> sind oder mit dem <u>Christentum</u> zu tun haben, nach rechts.<br><br><u>Tun Sie das so schnell wie möglich.</u>")
	aCategories.push("Wischen Sie Bilder oder Begriffe, die <u>unangenehm</u> sind oder mit <u>dicken Menschen</u> zu tun haben, nach links. Wischen Sie Bilder oder Begriffe, die <u>angenehm</u> sind oder mit <u>dünnen Menschen</u zu tun haben, nach rechts.<br><br><u>Tun Sie das so schnell wie möglich.</u>")
	aCategories.push("Wischen Sie Bilder oder Begriffe, die <u>unangenehm</u> sind oder mit <u>Behinderten</u> zu tun haben, nach links. Wischen Sie Bilder oder Begriffe, die <u>angenehm</u> sind oder mit <u>Nichtbehinderten</u> zu tun haben, nach rechts.<br><br><u>Tun Sie das so schnell wie möglich.</u>");


	var aCategories2 = []

	aCategories2.push("Wischen Sie Begriffe, die <u>angenehm</u> sind oder mit <u>berühmten Schwarzen</u> zu tun haben, nach links. Wischen Sie Begriffe, die <u>unangenehm</u> sind oder mit <u>berühmten Weißen</u> zu tun haben, nach rechts.<br><br><u>Tun Sie das so schnell wie möglich.</u>")
	aCategories2.push("Wischen Sie Bilder oder Begriffe, die Sie mit <u>Frauen oder Arbeit</u> verbinden, nach links. Wischen Sie Bilder oder Begriffe, die Sie mit <u>Männern oder Familie</u> verbinden, nach rechts.<br><br><u>Tun Sie das so schnell wie möglich.</u>")
	aCategories2.push("Wischen Sie Bilder oder Begriffe, die <u>angenehm</u> sind oder mit <u>Homosexuellen</u> zu tun haben, nach links. Wischen Sie Bilder oder Begriffe, die <u>unangenehm</u> sind oder mit <u>Heterosexuellen</u> zu tun haben, nach rechts.<br><br><u>Tun Sie das so schnell wie möglich.</u>")
	aCategories2.push("Wischen Sie Bilder oder Begriffe, die <u>angenehm</u> sind oder mit <u>alten Menschen</u> zu tun haben, nach links. Wischen Sie Bilder oder Begriffe, die <u>unangenehm</u> sind oder mit <u>jungen Menschen</u> zu tun haben, nach rechts.<br><br><u>Tun Sie das so schnell wie möglich.</u>")
	aCategories2.push("Wischen Sie Bilder oder Begriffe, die <u>angenehm</u> sind oder mit dem <u>Islam</u> zu tun haben, nach links. Wischen Sie Bilder oder Begriffe, die <u>unangenehm</u> sind oder mit dem <u>Christentum</u> zu tun haben, nach rechts.<br><br><u>Tun Sie das so schnell wie möglich.</u>")
	aCategories2.push("Wischen Sie Bilder oder Begriffe, die <u>angenehm</u> sind oder mit <u>dicken Menschen</u> zu tun haben, nach links. Wischen Sie Bilder oder Begriffe, die <u>unangenehm</u> sind oder mit <u>dünnen Menschen</u> zu tun haben, nach rechts.<br><br><u>Tun Sie das so schnell wie möglich.</u>")
	aCategories2.push("Wischen Sie Bilder oder Begriffe, die <u>angenehm</u> sind oder mit <u>Behinderten</u> zu tun haben, nach links. Wischen Sie Bilder oder Begriffe, die <u>unangenehm</u> sind oder mit <u>Nichtbehinderten</u> zu tun haben, nach rechts.<br><br><u>Tun Sie das so schnell wie möglich.</u>");
}
	
var aTypes = new Array();

if(lang == "en") {
	aTypes.push({name:"I'm not racist, but...", id:0});
	aTypes.push({name:"I'm not sexist, but...", id:1});
	aTypes.push({name:"I'm not homophobic, but...", id:2});
	aTypes.push({name:"I don't-mind old-people, but...", id:3});
	aTypes.push({name:"I'm not Islamophobic, but...", id:4});
	aTypes.push({name:"I don't-mind fat-people, but...", id:5});
	aTypes.push({name:"I don't-mind disabled-people, but...", id:6});
} else if(lang =="fr"){
	aTypes.push({name:"Je-ne suis-pas raciste, mais...", id:0});
	aTypes.push({name:"Je-ne suis-pas sexiste, mais...", id:1});
	aTypes.push({name:"Je-ne suis-pas homophobe, mais...", id:2});
	aTypes.push({name:"Je-n'ai rien-contre les-vieux, mais...", id:3});
	aTypes.push({name:"Je-ne suis-pas islamophobe, mais...", id:4});
	aTypes.push({name:"Je-n'ai rien-contre les-gros, mais...", id:5});
	aTypes.push({name:"Je-n'ai rien-contre les-personnes-handicapées, mais...", id:6});
} else if(lang =="de"){
	aTypes.push({name:"Ich bin nicht rassistisch, aber-...", id:0});
	aTypes.push({name:"Ich bin nicht sexistisch, aber-...", id:1});
	aTypes.push({name:"Ich bin nicht homophob, aber-...", id:2});
	aTypes.push({name:"Ich habe nichts gegen alte Menschen, aber-...", id:3});
	aTypes.push({name:"Ich habe nichts gegen Muslime, aber-...", id:4});
	aTypes.push({name:"Ich habe nichts gegen dicken Menschen, aber-...", id:5});
	aTypes.push({name:"Ich habe nichts gegen Behinderte, aber-...", id:6});
}







var aFollowing = new Object();
aFollowing.en = "On the following screens, you'll be asked to sort words into groups.<br><br> <u>Do this as fast as you can</u>.<br><br>If you make a mistake, this guy will appear<br><br><img src=images/icn_fail.png width='82' height='83'/>"
aFollowing.fr = "Sur les écrans suivants, nous vous demanderons de classer des mots dans des catégories.<br><br><u>Allez aussi vite que vous le pouvez.</u><br><br> Si vous faites une erreur, ce personnage apparaîtra.<br><br><img src=images/icn_fail.png width='82' height='83'/>"
aFollowing.de = "Auf den folgenden Bildschirmen sollen Sie Begriffe in Kategorien einteilen.<br><br><u>Tun Sie das so schnell Sie können.</u><br><br>Wenn Sie einen Fehler begehen, taucht diese Person auf.<br><br><img src=images/icn_fail.png width='82' height='83'/>"


var aEverything = new Object();
aEverything["en"] = "<div>Everything, from our social media posts to the news we consume and our relationships are influenced by our unconscious biases.<br><img src='images/spacer.gif' height='8'><br><img src='images/white.png' height='1' width='30' align='top'><br><img src='images/spacer.gif' height='8'><br>How biased do you think you are towards ";
aEverything["fr"] = "<div>Tout, de notre comportement sur les réseaux sociaux à nos relations avec les autres, en passant par l'information que nous consommons, est influencé par nos préjugés inconscients.<br><img src='images/spacer.gif' height='8'><br><img src='images/white.png' height='1' width='30' align='top'><br><img src='images/spacer.gif' height='8'><br>Dans quelle mesure croyez-vous entretenir des préjugés envers les ";
aEverything["de"] = "<div>Alles – von unserem Verhalten in sozialen Netzwerken über unsere Beziehungen bis hin zu den Nachrichten, die wir konsumieren – wird von unseren unbewussten Vorurteilen beeinflusst.<br><img src='images/spacer.gif' height='8'><br><img src='images/white.png' height='1' width='30' align='top'><br><img src='images/spacer.gif' height='8'><br>Wie voreingenommen sind Sie gegenüber ";


var aConclusion = new Object();
aConclusion["en"] = "<span class='conclusion_text'>It can take a lifetime to make a snap decision.</span><br><img src='images/spacer.gif' width='1' height='10'><br><span class='conclusion_text2'>Is your mind made up?</span><br><br><br><img src='images/icn_eye.png' width='134'><br><br><br><a href='#' class='conclusion_links' onclick='showHow();'>Find out how this works</a><br><a href='#' class='conclusion_links' onclick='closeConclusion()'>Try another theme</a><br><a href='#' class='conclusion_links' onclick='showCredits();'>Credits</a>"
aConclusion["fr"] ="<span class='conclusion_text'>C'est toute une vie qui informe nos réflexes.</span><br><img src='images/spacer.gif' width='1' height='10'><br><span class='conclusion_text2'>Votre jugement vous joue-t-il des tours?</span><br><br><br><img src='images/icn_eye.png' width='134'><br><br><br><a href='#' class='conclusion_links' onclick='showHow();'>Comment ça marche?</a><br><a href='#' class='conclusion_links' onclick='closeConclusion()'>Testez un autre thème</a><br><a href='#' onclick='showCredits()' class='conclusion_links'>Générique</a>"
aConclusion["de"] ="<span class='conclusion_text'>Man kann ein ganzes Leben brauchen, um eine schnelle Entscheidung zu treffen.</span><br><img src='images/spacer.gif' width='1' height='10'><br><span class='conclusion_text2'>Haben Sie sich entschieden?</span><br><br><br><img src='images/icn_eye.png' width='134'><br><br><br><a href='#' class='conclusion_links' onclick='showHow();'>WIE FUNKTIONIERT DER TEST?</a><br><a href='#' class='conclusion_links' onclick='closeConclusion()'>anderes Thema testen</a><br><a href='#' onclick='showCredits()' class='conclusion_links'>MITWIRKENDE</a>"

function getOpposite1() {
	var aOpposite1 = new Object()
aOpposite1.en = "Ok, now let’s try the opposite.<br><br>"+aCategories[currentSubjectId];
	aOpposite1.fr = "Parfait! Maintenant, tentons de faire le contraire.<br><br>"+aCategories[currentSubjectId];
	aOpposite1.de = "Ausgezeichnet! Nun machen wir das Gegenteil.<br><br>"+aCategories[currentSubjectId];
	return aOpposite1[lang]
}

function getOpposite2() {
	var aOpposite2 = new Object();
	aOpposite2.en = "Ok, now let's try the opposite.<br><br>"+aCategories2[currentSubjectId];
	aOpposite2.fr = "Parfait! Maintenant, tentons de faire le contraire.<br><br>"+aCategories2[currentSubjectId];
	aOpposite2.de = "Ausgezeichnet! Nun machen wir das Gegenteil.<br><br>"+aCategories2[currentSubjectId];
	
	return aOpposite2[lang]
}

function getNice1() {
	var aNice1 = new Object();
aNice1.en = "Nice! Now let's try another round, this time with some associations.<br><br>"+aCategories[currentSubjectId]
aNice1.fr = "Parfait! Essayons maintenant de faire la même chose avec quelques associations. <br><br>"+aCategories[currentSubjectId]
aNice1.de = "Sehr gut! Nun tun wir das Gleiche mit ein paar Assoziationen.<br><br>"+aCategories[currentSubjectId]
	return aNice1[lang]
}

function getNice2() {
	var aNice2 = new Object();
aNice2.en = "Nice! Now let's try another round, this time with some associations.<br><br>"+aCategories2[currentSubjectId]
aNice2.fr = "Parfait! Essayons maintenant de faire la même chose avec quelques associations. <br><br>"+aCategories2[currentSubjectId]
aNice2.de = "Sehr gut! Nun tun wir das Gleiche mit ein paar Assoziationen.<br><br>"+aCategories2[currentSubjectId]
	return aNice2[lang]
}

function getResult1(_biasType) {
	var aResult1 = new Object();
aResult1["en"] = "<img src=images/icn_sad.png width='82' height='83'/><br><br><span style='font-family: \"MotifLight\", sans-serif;'>Hmm... Looks like you're more biased towards "+aSubjectTopics[currentSubjectId][0]+" than you thought.</span>"
aResult1["fr"] = "<img src=images/icn_sad.png width='82' height='83'/><br><br><span style='font-family: \"MotifLight\", sans-serif;'>Hmm... On dirait que vous avez plus de préjugés envers les "+aSubjectTopics[currentSubjectId][0]+" que vous ne le pensiez.</span>"
aResult1["de"] = "<img src=images/icn_sad.png width='82' height='83'/><br><br><span style='font-family: \"MotifLight\", sans-serif;'>Tja, scheint so, als hätten Sie mehr Vorurteile gegenüber "+aSubjectTopics[currentSubjectId][0]+", als Sie dachten.</span>"

	return aResult1[lang]
}

function getResult2(_biasType) {
	var aResult2 = new Object();
aResult2["en"] = "<img src=images/icn_happy.png width='82' height='83'/><br><br><span style='font-family: \"MotifLight\", sans-serif;'>Hmm... Looks like you're less biased towards "+aSubjectTopics[currentSubjectId][0]+" than you thought.</span>"
aResult2["fr"] = "<img src=images/icn_happy.png width='82' height='83'/><br><br><span style='font-family: \"MotifLight\", sans-serif;'>Hmm... On dirait que vous avez moins de préjugés envers les "+aSubjectTopics[currentSubjectId][0]+" que vous ne le pensiez.</span>"
aResult2["de"] = "<img src=images/icn_happy.png width='82' height='83'/><br><br><span style='font-family: \"MotifLight\", sans-serif;'>Tja, scheint so, als hätten Sie weniger Vorurteile gegenüber "+aSubjectTopics[currentSubjectId][0]+", als Sie dachten.</span>"

	return aResult2[lang]
}

function getResult3(_biasType, _biasnum) {
	var aResult3 = new Object();
	var imgsrc = "neutral";
	if(_biasnum <= 5) {
		imgsrc = "happy";
	}
aResult3["en"] = "<img src=images/icn_"+imgsrc+".png width='82' height='83'/><br><br><span style='font-family: \"MotifLight\", sans-serif;'>You're right, looks like your are "+_biasType+ "biased towards "+aSubjectTopics[currentSubjectId][0] +".</span>"
if(_biasnum < 20) {
	aResult3["fr"] = "<img src=images/icn_"+imgsrc+".png width='82' height='83'/><br><br><span style='font-family: \"MotifLight\", sans-serif;'>Vous aviez raison. Vous n'avez "+_biasType+ "de préjugés envers les "+aSubjectTopics[currentSubjectId][0] +".</span>"

} else {
	aResult3["fr"] = "<img src=images/icn_"+imgsrc+".png width='82' height='83'/><br><br><span style='font-family: \"MotifLight\", sans-serif;'>Vous aviez raison. Vous avez "+_biasType+ "de préjugés envers les "+aSubjectTopics[currentSubjectId][0] +".</span>"

}
aResult3["de"] = "<img src=images/icn_"+imgsrc+".png width='82' height='83'/><br><br><span style='font-family: \"MotifLight\", sans-serif;'>Sie hatten Recht: Sie sind "+_biasType+ "voreingenommen gegenüber "+aSubjectTopics[currentSubjectId][0] +".</span>"
	return aResult3[lang]
}

function getConclusion(){
	return aConclusion[lang]
}


//shuffle(aTypes);	
var headerHeight =0;
var aSubjects = ["racism", "sexism", "homophobia", "ageism","religion","weight","handicap"];
var currentSubjectId = -1;
var smallScreen = false;
var screenHeight = 0
var landscapeOnLoad = false;
$( document ).ready(function() {
	//document.ontouchmove = function(e){ e.preventDefault(); }
	 if (window.innerHeight > window.innerWidth) {
		$("#portrait_hide").css("display","none");
	  } else {

		  landscapeOnLoad = true;
		  $("#portrait_hide").css("display","block");

	  }
	if($("#header").innerHeight() != undefined) {
	//	console.log($("#header"))
		headerHeight = $("#header").innerHeight()
		//console.log("headerHeight "+headerHeight)
		screenWidth = $(window).width();
	screenHeight = $(window).height();
	$("#contour").css("width", screenWidth-20);
	$("#contour").css("height", $(window).height()-(headerHeight+20));
	$("#contour").css("top", headerHeight);
	$(".tiny").css("top", Math.abs(40-headerHeight));
	$("#how").css("top", headerHeight);
	$("#how_close").css("top", headerHeight-20);
	$("#credits_close").css("top", headerHeight-20);
	
	TweenMax.to($("#contour"), 0, {scale:1.06,ease:Quad.easeIn,overwrite:true,transformOrigin:"center"});
	} else {
		setTimeout(setArteHeader,500)
		
	}
	
	
	console.log($(window).height())
	if($(window).height() < 600) {
		
		//smallScreen = true;
	}
	
	window.onresize = function (event) {
  applyOrientation();
}

	
	//setTimeout(init,5000);
	$(window).bind('hashchange', function() {
 console.log("change")
 location.reload();
});

});

function applyOrientation() {
	if(landscapeOnLoad) {
		landscapeOnLoad = false;
		location.reload();
	} else {
  if (window.innerHeight > window.innerWidth) {
    $("#portrait_hide").css("display","none");
  } else {

	  
	  $("#portrait_hide").css("display","block");
	
  }
	}
}

function setArteHeader() {
	headerHeight = $("#arte-header").height()
	console.log("arte "+headerHeight)
	screenWidth = $(window).width();
	screenHeight = $(window).height();
	$("#contour").css("width", screenWidth-20);
	$("#contour").css("height", $(window).height()-(headerHeight+20));
	$("#contour").css("top", headerHeight);
	$(".tiny").css("top", Math.abs(40-headerHeight));
	$("#how").css("top", headerHeight);
	$("#how_close").css("top", headerHeight-20);
	$("#credits_close").css("top", headerHeight-20);
	
	
	//$("#swipe_0").css("top", headerHeight);
	//$("#swipe_1").css("top", headerHeight);
	TweenMax.to($("#contour"), 0, {scale:1.06,ease:Quad.easeIn,overwrite:true,transformOrigin:"center"});
}

function initIntro() {
	
	//setTimeout(randomSubject, delay*1000)
}

function showHow() {
	$("#how").css("display", "block");
	$("#how_header").css("display", "block");
	$("#how_close").css("display", "block");
}

function showCredits() {
	$("#credits").css("display", "block");
	$("#credits_header").css("display", "block");
	$("#credits_close").css("display", "block");
}

function closeHow() {
	$("#how").css("display", "none");
	$("#how_header").css("display", "none");
	$("#how_close").css("display", "none");
}

function closeCredits() {
	$("#credits").css("display", "none");
	$("#credits_header").css("display", "none");
	$("#credits_close").css("display", "none");
}

function init() {
	if(Math.random() > .5) {
		flip = true;
	} else {
		flip = false;
	}
	//flip = false;
	console.log("init")
	var delay = 1;
	//$(".logogif").attr("src","images/bias_02.gif");
	//TweenMax.to($(".introtext"), 0, {top:"50%", ease:Quad.easeOut,delay:delay});
	
	TweenMax.to($(".logoimage"), 2, {autoAlpha:1, ease:Quad.easeOut,delay:delay});
	
	//TweenMax.to($(".logoimage"), 1.7, {top:screenHeight-52, ease:Expo.easeIn,delay:delay+1});
	
	delay+=3;
	TweenMax.to($(".logoimage"), 2, {autoAlpha:0, ease:Expo.easeInOut,delay:delay});
	TweenMax.to($(".tiny"), 0.6, {autoAlpha:0,ease:Quad.easeOut, delay:delay});
	TweenMax.to($("#swipe_left"), .6, {autoAlpha:0, ease:Quad.easeOut,delay:delay});
	TweenMax.to($("#swipe_right"), .6, {autoAlpha:0, ease:Quad.easeOut,delay:delay});
	
	delay+=2
	TweenMax.to($("#contour"), 0, {opacity:1});
	
	TweenMax.to($("#contour"), .7, {scale:1,ease:Quad.easeOut,delay:delay, transformOrigin:"center"});
	
	delay+=1;
	TweenMax.to($(".introtext"), 1, {autoAlpha:1, ease:Quad.easeOut,delay:delay,overwrite:true});
	//delay+=1;
	//TweenMax.to($(".introtext"), .7, {top:90, ease:Expo.easeOut,delay:delay});
	
	delay-=.5;
	
	
	
	setTimeout(randomSubject, delay*1000)
	delay+=7
	
	//setTimeout(showInstructions, delay*1000)
	/*TweenMax.to($("#swipe_2"), 0.5, {x:0, y:-$(window).height(), ease:Expo.easeIn,overwrite:true});

	window.onkeyup = function(e) {
		var key = e.keyCode ? e.keyCode : e.which;
		if (key == 37) {
			console.log("left");
		  unlikeIt(0);
		}else if (key == 39) {
			console.log("right");
		   likeIt(1);					}
	}
	draggers[0] = Draggable.create("#swipe_0", {type:"x", edgeResistance:0.65, minimumMovement:0, throwProps:true, onDragStart:setDragStart, onDragEnd:checkDragDistance, onClick:setOnClick});
	draggers[1] = Draggable.create("#swipe_1", {type:"x", edgeResistance:0.65, minimumMovement:0, throwProps:true, onDragStart:setDragStart, onDragEnd:checkDragDistance, onClick:setOnClick});
	setNextStatement();
	startTimer();*/
	initIcons();
}
var currentIcon = "";
function iconDone(_icon) {
	console.log("icon done")
	var delay =2.5 ;
	TweenMax.to($(".introtext"), .8, {y:-500, autoAlpha:1, ease:Expo.easeInOut, delay:delay});
	TweenMax.to($("#icon_"+_icon), .8, {y:-500, autoAlpha:0, ease:Expo.easeInOut, delay:delay+0.1, transformOrigin:"94px 94px"});
	
	delay+=.1;
	TweenMax.to($(".type"), 1, {y:-500, autoAlpha:0, ease:Expo.easeInOut, delay:delay, transformOrigin:"center"});
	delay+=.5;
	if(firstTime) {
	setTimeout(showInstructions, delay*1000)
	} else {
		setTimeout(showInstructions3, delay*1000)
	
	}
	//delay+=.2
	$("#icon_"+_icon).css( "zIndex", 10005 );
	$(".type").css( "zIndex", 10006 );
	
}

function setDragStartSlider() {
		

}


function setBias() {
		TweenMax.to($("#okslide"), 1, {autoAlpha:1, ease:Quad.easeOut,overwrite:true});

}

function setSliderOnClick() {
	alert("click")
}

function activateDrag() {
	Draggable.create("#drop", {type:"x", minimumMovement:1, edgeResistance:0.65, minimumMovement:0, throwProps:false, onDragStart:setDragStartSlider, onDragEnd:setBias});
	
}

function traceHeight() {
	var delay =0;
	console.log("innerHeight2 "+$("#swipe_2").children(".statement_indications").children(".indications").innerHeight())
	TweenMax.to($("#swipe_2").children(".statement_indications").children(".indications"), 0, {top:(screenHeight-95) / 2 - ($("#swipe_2").children(".statement_indications").children(".indications").innerHeight()+105)/2});
	//TweenMax.to($(".ui-slider-track"), 0.6, {autoAlpha:1,delay:delay,ease:Quad.easeOut,overwrite:true});
	TweenMax.to($(".ui-slider-track"), 0, {top:$("#swipe_2").children(".statement_indications").children(".indications").innerHeight()+$("#swipe_2").children(".statement_indications").children(".indications").position().top+45});
	TweenMax.to($("#slider_text"), 0, {top:$("#swipe_2").children(".statement_indications").children(".indications").innerHeight()+$("#swipe_2").children(".statement_indications").children(".indications").position().top+20});
	
	var diffWidth = screenWidth - 250;
	
	$("#slider_not").css("marginLeft", diffWidth/2-10)
	$("#slider_yes").css("marginLeft", -(diffWidth/2))
	
}

function showInstructions() {
	
	var delay = 1;
	
	$("#swipe_2").children(".statement_indications").children(".indications").html(aEverything[lang]+aSubjectTopics[currentSubjectId][0]+"?");
	//if(smallScreen) {
	console.log("screenHeight "+screenHeight)
	console.log("innerHeight "+$("#swipe_2").children(".statement_indications").children(".indications").innerHeight())
	setTimeout(traceHeight, 100)
	//$("#swipe_2").children(".statement_indications").children(".indications").css("top", (screenHeight-95) / 2 - ($("#swipe_2").children(".statement_indications").children(".indications").innerHeight()+105)/2)
	
	TweenMax.to($("#swipe_2").children(".statement_indications").children(".indications"), 0, {top:(screenHeight-95-headerHeight) / 2 - ($("#swipe_2").children(".statement_indications").children(".indications").innerHeight()+105)/2+headerHeight});
	
	
	slideInterval = setInterval(checkSliderValue, 100)
	
	//	$("#swipe_2").children(".statement_indications").children(".indications").css("top", $(".type").innerHeight()-30)
	//} else {
		//$("#swipe_2").children(".statement_indications").children(".indications").css("top", 100);
	//}
	

	$("#swipe_2").css("display", "block")
	
	TweenMax.to($("#swipe_2"), 0, {autoAlpha:0});

	TweenMax.to($("#swipe_2").children(".statement_indications").children(".indications"), 0, {left:(screenWidth)/2-$("#swipe_2").children(".statement_indications").children(".indications").innerWidth()/2});
		
		
	TweenMax.to($("#swipe_2"), 1, {autoAlpha:1, ease:Quad.easeOut,overwrite:true});

	TweenMax.to($("#swipe_2").children(".statement_indications"), 0.6, {autoAlpha:1,delay:delay,ease:Quad.easeOut,overwrite:true});
	TweenMax.to($("#slider_text"), 0.6, {autoAlpha:1,delay:delay,ease:Quad.easeOut,overwrite:true});
	
	TweenMax.to($(".ui-slider-track"), 0.6, {autoAlpha:1,delay:delay,ease:Quad.easeOut,overwrite:true});
	TweenMax.to($(".ui-slider-track"), 0, {top:$("#swipe_2").children(".statement_indications").children(".indications").innerHeight()+$("#swipe_2").children(".statement_indications").children(".indications").position().top+45});
	TweenMax.to($("#slider_text"), 0, {top:$("#swipe_2").children(".statement_indications").children(".indications").innerHeight()+$("#swipe_2").children(".statement_indications").children(".indications").position().top+20});
	
	var diffWidth = screenWidth - 250;
	
	$("#slider_not").css("marginLeft", diffWidth/2-10)
	$("#slider_yes").css("marginLeft", -(diffWidth/2))
	
	
	TweenMax.to($(".logo"), 1, {autoAlpha:0, ease:Quad.easeOut,delay:.5, onComplete:function(){$(".logo").css("display","none")}});
	
}
var sliderValue = 0;
function checkSliderValue() {
	sliderValue = $(".ui-slider-handle").attr("aria-valuenow");
	console.log("slider value " +sliderValue)
	if(sliderValue != 25) {
		clearInterval(slideInterval);
		setBias();
	}
}

var slideInterval;
function gotIt() {
	var delay=.5
	TweenMax.to($("#swipe_2").children(".statement_indications"), 0.5, {autoAlpha:0,delay:delay,ease:Quad.easeOut, onComplete:swapText0});
	TweenMax.to($(".ui-slider-track"), 0.5, {autoAlpha:0,delay:delay,ease:Quad.easeOut,overwrite:true});
	TweenMax.to($("#slider_text"), 0.5, {autoAlpha:0,delay:delay,ease:Quad.easeOut,overwrite:true});
			
	TweenMax.to($("#okslide"), .5, {autoAlpha:0, ease:Quad.easeOut,overwrite:true});
sliderValue = $(".ui-slider-handle").attr("aria-valuenow");
	
}


function showInstructions3() {
	swapText1();
	var delay = 1;

	//if(smallScreen) {
		
	//	$("#swipe_2").children(".statement_indications").children(".indications").css("top", $(".type").innerHeight()-30)
	//} else {
		//$("#swipe_2").children(".statement_indications").children(".indications").css("top", 100);
	//}
	

	$("#swipe_2").css("display", "block")
	
	TweenMax.to($("#swipe_2"), 0, {autoAlpha:0});

	TweenMax.to($("#swipe_2").children(".statement_indications").children(".indications"), 0, {left:(screenWidth)/2-$("#swipe_2").children(".statement_indications").children(".indications").innerWidth()/2});
		
		
	TweenMax.to($("#swipe_2"), 1, {autoAlpha:1, ease:Quad.easeOut,overwrite:true});

	TweenMax.to($("#swipe_2").children(".statement_indications"), 0.6, {autoAlpha:1,delay:delay,ease:Quad.easeOut,overwrite:true});
	
	TweenMax.to($(".logo"), 1, {autoAlpha:0, ease:Quad.easeOut,delay:.5, onComplete:function(){$(".logo").css("display","none")}});
	//delay+=8
	//TweenMax.to($("#swipe_2").children(".statement_indications"), 0.5, {autoAlpha:0,delay:delay,ease:Quad.easeOut, onComplete:swapText0});
	delay+=1
	TweenMax.to($(".okbutton"), 1, {autoAlpha:1, ease:Quad.easeOut,overwrite:true, delay:delay});
	
}


function swapText0() {
	
	var delay =0;
	$("#swipe_2").children(".statement_indications").children(".indications").html(aFollowing[lang])
	//if(smallScreen) {
		
	//	$("#swipe_2").children(".statement_indications").children(".indications").css("top", $(".type").innerHeight()-30)
	//} else {
		//$("#swipe_2").children(".statement_indications").children(".indications").css("top", 100)
	//}
	
	setTimeout(setPos3, 10)
	
	console.log("show instructions");
	//$("#swipe_2").child("statement_indications").child("indications").left = 
	$("#swipe_2").css("display", "block")
	TweenMax.to($("#swipe_2").children(".statement_indications").children(".indications"), 0, {left:(screenWidth)/2-$("#swipe_2").children(".statement_indications").children(".indications").innerWidth()/2});
	
	//TweenMax.to($("#swipe_2"), 0, {autoAlpha:0});

	TweenMax.to($("#swipe_2").children(".statement_indications").children(".indications"), 0, {top:(screenHeight)/2-$("#swipe_2").children(".statement_indications").children(".indications").innerHeight()/2});
		
		
	TweenMax.to($("#swipe_2"), 1, {autoAlpha:1, ease:Quad.easeOut,overwrite:true});

	TweenMax.to($("#swipe_2").children(".statement_indications"), 0.6, {autoAlpha:1,delay:delay,ease:Quad.easeOut,overwrite:true});
	//delay+=10;
	delay+=1
	TweenMax.to($(".nextbutton"), 1, {autoAlpha:1, ease:Quad.easeOut,overwrite:true, delay:delay});
}

function setPos3() {
	TweenMax.to($("#swipe_2").children(".statement_indications").children(".indications"), 0, {top:(screenHeight-95-headerHeight)/2-$("#swipe_2").children(".statement_indications").children(".indications").innerHeight()/2+headerHeight});

}

function nextIntro() {
	var delay =0;
	TweenMax.to($(".nextbutton"), .5, {autoAlpha:0, ease:Quad.easeOut,overwrite:true, delay:delay});
	delay+=.4
	TweenMax.to($("#swipe_2").children(".statement_indications"), 0.5, {autoAlpha:0,delay:delay,ease:Quad.easeOut, onComplete:swapText1});
	delay+=.6
	TweenMax.to($("#swipe_2").children(".statement_indications"), 0.5, {autoAlpha:1,delay:delay,ease:Quad.easeOute});
	delay+=1
	TweenMax.to($(".okbutton"), 1, {autoAlpha:1, ease:Quad.easeOut,overwrite:true, delay:delay});
}

function swapText1() {
	
	//if(smallScreen) {
		
	//	$("#swipe_2").children(".statement_indications").children(".indications").css("top", $(".type").position().top-60)
	//} else {
		//$("#swipe_2").children(".statement_indications").children(".indications").css("top", 100)
	//}
	
	var sNow = "";
	console.log("swapText1 "+flip + " -- "+currentSession)
	
	if(!flip) {
		if(currentSession == 1) {
			$("#swipe_2").children(".statement_indications").children(".indications").html(getOpposite1())
			if(currentSubjectId == 1) {
				setTimeout(function() {$("#swipe_left").text(aWords[currentSubjectId][0]+ " — "+aContext2[1])
				$("#swipe_right").text(aWords[currentSubjectId][1]+ " — "+aContext2[0])}, 1000)
			} else {
				setTimeout(function() {$("#swipe_left").text(aWords[currentSubjectId][0]+ " — "+aContext[1])
				$("#swipe_right").text(aWords[currentSubjectId][1]+ " — "+aContext[0])}, 1000)
			}
		} else if(currentSession == 0) {
			$("#swipe_2").children(".statement_indications").children(".indications").html(getNice1())
			
			if(currentSubjectId == 1) {
			
				setTimeout(function() {$("#swipe_left").text(aWords[currentSubjectId][0]+ " — "+aContext2[0])
				$("#swipe_right").text(aWords[currentSubjectId][1]+ " — "+aContext2[1])}, 1000)
			} else {
				setTimeout(function() {$("#swipe_left").text(aWords[currentSubjectId][0]+ " — "+aContext[0])
				$("#swipe_right").text(aWords[currentSubjectId][1]+ " — "+aContext[1])}, 1000)
			}
			
		} else if(currentSession == -1) {
			$("#swipe_2").children(".statement_indications").children(".indications").html(aWarmup[currentSubjectId])
			
			setTimeout(function() {$("#swipe_left").text(aWords[currentSubjectId][0]); $("#swipe_right").text(aWords[currentSubjectId][1])}, 1000)
			
		}
	} else {
		if(currentSession == 1) {
			$("#swipe_2").children(".statement_indications").children(".indications").html(getOpposite2())
			if(currentSubjectId == 1) {
				setTimeout(function() {$("#swipe_left").text(aWords[currentSubjectId][0]+ " — "+aContext2[0])
				$("#swipe_right").text(aWords[currentSubjectId][1]+ " — "+aContext2[1])}, 1000)
			} else {
				setTimeout(function() {$("#swipe_left").text(aWords[currentSubjectId][0]+ " — "+aContext[0])
				$("#swipe_right").text(aWords[currentSubjectId][1]+ " — "+aContext[1])}, 1000)
			}
		} else if(currentSession == 0) {
			
			$("#swipe_2").children(".statement_indications").children(".indications").html(getNice2())
			if(currentSubjectId == 1) {
				setTimeout(function() {$("#swipe_left").text(aWords[currentSubjectId][0]+ " — "+aContext2[1])
				$("#swipe_right").text(aWords[currentSubjectId][1]+ " — "+aContext2[0])}, 1000)
			} else {
					setTimeout(function() {$("#swipe_left").text(aWords[currentSubjectId][0]+ " — "+aContext[1])
				$("#swipe_right").text(aWords[currentSubjectId][1]+ " — "+aContext[0])}, 1000)
			}
		} else if(currentSession == -1) {
			$("#swipe_2").children(".statement_indications").children(".indications").html(aWarmup[currentSubjectId])
			setTimeout(function() {$("#swipe_left").text(aWords[currentSubjectId][0]); $("#swipe_right").text(aWords[currentSubjectId][1])}, 1000)
		}
	}
	
	if($("#swipe_2").children(".statement_indications").children(".indications").offset().top + $("#swipe_2").children(".statement_indications").children(".indications").outerHeight(true) > $(".okbutton").offset().top) {
		console.log("make it smaller")
	}
	console.log("top "+($("#swipe_2").children(".statement_indications").children(".indications").offset().top));
	
	TweenMax.to($("#swipe_2").children(".statement_indications").children(".indications"), 0, {top:(screenHeight-95-headerHeight)/2-$("#swipe_2").children(".statement_indications").children(".indications").innerHeight()/2+20+headerHeight});
	setTimeout(setY2, 10);
	console.log("innerheight "+($("#swipe_2").children(".statement_indications").children(".indications").outerHeight(true)));
	console.log("okbutton "+($(".okbutton").offset().top));
	
}

function setY2() {
	TweenMax.to($("#swipe_2").children(".statement_indications").children(".indications"), 0, {top:(screenHeight-95-headerHeight)/2-$("#swipe_2").children(".statement_indications").children(".indications").innerHeight()/2+20+headerHeight});

}

function randomSubject() {
	
	currentSubjectId = Math.round(Math.random()*(aSubjects.length-1));
	//currentSubjectId = 1;
	var counter = 0
	while(aSubjects[currentSubjectId] == null && counter < 30) {
		currentSubjectId = Math.round(Math.random()*(aSubjects.length-1));	
		counter++
	}
	if(counter == 30) {
		aSubjects = ["racism", "sexism", "homophobia", "ageism","religion","weight","handicap"];
		currentSubjectId = Math.round(Math.random()*(aSubjects.length-1));
	
	}
	//currentSubjectId =5
	//currentSubjectId = 0;
	console.log("currentSubjectId "+currentSubjectId)
	//currentSubjectId = 6
	
	loadPicto(aSubjects[currentSubjectId]);
	if(aSubjects[currentSubjectId] == "racism") {
		aStatements = aStatements_racism;
		aStatementBack = aStatementBack_racism
		
	} else if(aSubjects[currentSubjectId] == "sexism") {
		aStatements = aStatements_sexism; 
		aStatementBack = aStatementBack_sexism
		
	} else if(aSubjects[currentSubjectId] == "homophobia") {
		aStatements = aStatements_homophobia; 
		aStatementBack = aStatementBack_homophobia
		
	}else if(aSubjects[currentSubjectId] == "ageism") {
		aStatements = aStatements_ageism; 
		aStatementBack = aStatementBack_ageism
		
	}else if(aSubjects[currentSubjectId] == "religion") {
		aStatements = aStatements_religion; 
		aStatementBack = aStatementBack_religion
		
	}else if(aSubjects[currentSubjectId] == "weight") {
		aStatements = aStatements_weight; 
		aStatementBack = aStatementBack_weight
		
	}else if(aSubjects[currentSubjectId] == "weapons") {
		aStatements = aStatements_weapons; 
		aStatementBack = aStatementBack_weapons
		
	}else if(aSubjects[currentSubjectId] == "handicap") {
		aStatements = aStatements_handicap; 
		aStatementBack = aStatementBack_handicap
		
	}
	console.log("currentSubjectId "+currentSubjectId)
	
	console.log("random "+aSubjects[currentSubjectId])
	
	//$(".type").css("top","50%");
	var sType = "";
	
		console.log("smallscreen "+smallScreen)
	if(smallScreen) {
		sType = aTypes[currentSubjectId].name.split("-").join(" ")
	} else {
		sType = aTypes[currentSubjectId].name.split(" ").join("<br>").split("-").join(" ")
		
	}
	$(".type").html(sType);
	$(".type").css("left","50%");
	$(".type").css("marginLeft",-$(".type").innerWidth()/2 );
	
	TweenMax.to($(".introtext"), 1, {autoAlpha:1, ease:Quad.easeOut,delay:0});
	
	TweenMax.to($(".type"), 1, {autoAlpha:1, ease:Quad.easeOut, overwrite:true, delay:2.4});
	
}



function closeInstructions1() {
	//okbutton
	//firstTime = false;
	var delay = 0;
	TweenMax.to($(".okbutton"), 0.6, {autoAlpha:0,ease:Quad.easeOut,overwrite:true, delay:delay});
	delay+=.2
	TweenMax.to($("#swipe_2").children(".statement_indications"), 0.6, {autoAlpha:0,ease:Quad.easeOut,overwrite:true, delay:delay});
	
	delay+=.2;
	TweenMax.to($(".type"), .6, {autoAlpha:0, ease:Quad.easeOut, overwrite:true, delay:delay});
	
	TweenMax.to($("#contour"), 0.6, {scale:1.06,ease:Quad.easeIn,overwrite:true,transformOrigin:"center"});
	
	delay+=.6
	TweenMax.to($(".tiny"), 0.6, {autoAlpha:1,ease:Quad.easeOut, delay:delay+.5});
	TweenMax.to($("#swipe_left"), .5, {autoAlpha:1, ease:Quad.easeOut,delay:delay+.5});
			TweenMax.to($("#swipe_right"), .5, {autoAlpha:1, ease:Quad.easeOut,delay:delay+.5});
	//TweenMax.to($("#swipe_2"), 0.5, {autoAlpha:0,ease:Quad.easeOut,overwrite:true});
	//TweenMax.to($("#contour"), 0.3, {scale:1.3,ease:Quad.easeOut,overwrite:true,transformOrigin:"center"});
	
	TweenMax.to($("#swipe_2"), 0.5, {x:0, y:-$(window).height(), ease:Expo.easeIn,overwrite:true, delay:delay});
	window.onkeyup = function(e) {
		var key = e.keyCode ? e.keyCode : e.which;
		if (key == 37) {
			console.log("left");
		  unlikeIt(0);
		}else if (key == 39) {
			console.log("right");
		   likeIt(1);					}
	}
	draggers[0] = Draggable.create("#swipe_0", {type:"x", edgeResistance:0.65, minimumMovement:0, throwProps:true, onDragStart:setDragStart, onDragEnd:checkDragDistance, onClick:setOnClick});
	draggers[1] = Draggable.create("#swipe_1", {type:"x", edgeResistance:0.65, minimumMovement:0, throwProps:true, onDragStart:setDragStart, onDragEnd:checkDragDistance, onClick:setOnClick});
	setNextStatement();
	
	startTimer();
}



function closeInstructions2() {
	//okbutton
	var delay = 0;
	
	TweenMax.to($(".okbutton"), 0.6, {autoAlpha:0,ease:Quad.easeOut,overwrite:true, delay:delay});
	delay+=.2
	TweenMax.to($("#swipe_3").children(".statement_indications"), 0.6, {autoAlpha:0,ease:Quad.easeOut,overwrite:true, delay:delay});
	
	delay+=.2;
	TweenMax.to($(".type"), .6, {autoAlpha:0, ease:Quad.easeOut, overwrite:true, delay:delay});
	
	TweenMax.to($("#contour"), 0.6, {scale:1.06,ease:Quad.easeIn,overwrite:true,transformOrigin:"center"});
	delay+=.6
	TweenMax.to($(".tiny"), 0.6, {autoAlpha:1,ease:Quad.easeOut, delay:delay+.5});
	TweenMax.to($("#swipe_left"), .5, {autoAlpha:1, ease:Quad.easeOut,delay:delay+.5});
			TweenMax.to($("#swipe_right"), .5, {autoAlpha:1, ease:Quad.easeOut,delay:delay+.5});
	//TweenMax.to($("#swipe_2"), 0.5, {autoAlpha:0,ease:Quad.easeOut,overwrite:true});
	//TweenMax.to($("#contour"), 0.3, {scale:1.3,ease:Quad.easeOut,overwrite:true,transformOrigin:"center"});
	
	TweenMax.to($("#swipe_3"), 0.5, {x:0, y:-$(window).height(), ease:Expo.easeIn,overwrite:true, delay:delay});
	setNextStatement();
	
	//startTimer();
}

function init2() {
	console.log("init2")
	var delay=0;
	TweenMax.to($("#swipe_3"), 0, {y:0,overwrite:true, delay:0});
	
	$("#swipe_3").css("display", "block")
//	if(smallScreen) {
		
	//	$("#swipe_3").children(".statement_indications").children(".indications").css("top", $(".type").position().top-60)
	//} else {
		//$("#swipe_3").children(".statement_indications").children(".indications").css("top", 100)
	//}
	
	if(!flip) {
		$("#swipe_3").children(".statement_indications").children(".indications").html(getOpposite2())
		
		if(currentSubjectId == 1) {
			setTimeout(function() {$("#swipe_left").text(aWords[currentSubjectId][0]+ " — "+aContext2[1])
			
		$("#swipe_right").text(aWords[currentSubjectId][1]+ " — "+aContext2[0])}, 1000)
		
		} else {
		setTimeout(function() {$("#swipe_left").text(aWords[currentSubjectId][0]+ " — "+aContext[1])
			
		$("#swipe_right").text(aWords[currentSubjectId][1]+ " — "+aContext[0])}, 1000)
		}
		
		//$("#swipe_left").text(aWords[currentSubjectId][0]+ " — "+aContext[1])
		//$("#swipe_right").text(aWords[currentSubjectId][1]+ " — "+aContext[0])
	} else {
		$("#swipe_3").children(".statement_indications").children(".indications").html(getOpposite1())
		if(currentSubjectId == 1) {
			setTimeout(function() {$("#swipe_left").text(aWords[currentSubjectId][0]+ " — "+aContext2[0])
			$("#swipe_right").text(aWords[currentSubjectId][1]+ " — "+aContext2[1])}, 1000)
		} else {
			setTimeout(function() {$("#swipe_left").text(aWords[currentSubjectId][0]+ " — "+aContext[0])
			$("#swipe_right").text(aWords[currentSubjectId][1]+ " — "+aContext[1])}, 1000)
		}
	}
	
	
	TweenMax.to($("#swipe_3").children(".statement_indications").children(".indications"), 0, {top:(screenHeight-95-headerHeight)/2-$("#swipe_3").children(".statement_indications").children(".indications").innerHeight()/2+headerHeight});
	TweenMax.to($("#swipe_3"), 0, {autoAlpha:0});

	TweenMax.to($("#swipe_3").children(".statement_indications").children(".indications"), 0, {left:(screenWidth)/2-$("#swipe_3").children(".statement_indications").children(".indications").innerWidth()/2});
		
	TweenMax.to($("#swipe_3").children(".statement_indications").children(".indications"), 0, {autoAlpha:0});
		
	//TweenMax.to($(".type"), 1, {autoAlpha:1, ease:Quad.easeOut, delay:delay});
	
	TweenMax.to($("#swipe_3"), 1, {autoAlpha:1, ease:Quad.easeOut,overwrite:true,delay:delay});
	TweenMax.to($(".tiny"), 0.6, {autoAlpha:0,ease:Quad.easeOut, delay:delay});
	TweenMax.to($("#swipe_left"), .5, {autoAlpha:0, ease:Quad.easeOut,delay:delay});
			TweenMax.to($("#swipe_right"), .5, {autoAlpha:0, ease:Quad.easeOut,delay:delay});
	delay+=.9;
	TweenMax.to($("#contour"), 0, {opacity:1});
	
	TweenMax.to($("#contour"), .7, {scale:1,ease:Quad.easeOut,delay:delay, transformOrigin:"center"});
	delay+=.5;
	TweenMax.to($("#swipe_3").children(".statement_indications"), 0, {autoAlpha:1, ease:Quad.easeOut,overwrite:true, delay:delay});
	
	TweenMax.to($("#swipe_3").children(".statement_indications").children(".indications"), .5, {autoAlpha:1, ease:Quad.easeOut,overwrite:true, delay:delay});
	delay+=.5
	
	TweenMax.to($(".okbutton"), .5, {autoAlpha:1, ease:Quad.easeOut,overwrite:true, delay:delay});
	delay+.5;
	aStatements = aStatementBack.slice();
	shuffle(aStatements);
	//setTimeout(setNextStatement, delay*1000);
}

function init3() {
	console.log("init3")
	var delay=0;
	TweenMax.to($("#swipe_3"), 0, {y:0,overwrite:true, delay:0});
	$("#swipe_3").css("display", "block")
	if(smallScreen) {		
		//$("#swipe_3").children(".statement_indications").children(".indications").css("top", $(".type").position().top-60)
	} else {
	//	$("#swipe_3").children(".statement_indications").children(".indications").css("top", $(".type").innerHeight()-20)
	}
	
	if(!flip) {
		$("#swipe_3").children(".statement_indications").children(".indications").html(getNice1())
		//$("#swipe_left").text(aWords[currentSubjectId][0]+ " — "+aContext[0])
		//$("#swipe_right").text(aWords[currentSubjectId][1]+ " — "+aContext[1])
		
		if(currentSubjectId == 1) {
			setTimeout(function() {$("#swipe_left").text(aWords[currentSubjectId][0]+ " — "+aContext2[0])
			$("#swipe_right").text(aWords[currentSubjectId][1]+ " — "+aContext2[1])}, 1000)
		} else {
			setTimeout(function() {$("#swipe_left").text(aWords[currentSubjectId][0]+ " — "+aContext[0])
			$("#swipe_right").text(aWords[currentSubjectId][1]+ " — "+aContext[1])}, 1000)
		}
		
	} else {
		$("#swipe_3").children(".statement_indications").children(".indications").html(getNice2())
		//$("#swipe_left").text(aWords[currentSubjectId][0]+ " — "+aContext[1])
		//$("#swipe_right").text(aWords[currentSubjectId][1]+ " — "+aContext[0])
		if(currentSubjectId == 1) {
			setTimeout(function() {$("#swipe_left").text(aWords[currentSubjectId][0]+ " — "+aContext2[1])
			$("#swipe_right").text(aWords[currentSubjectId][1]+ " — "+aContext2[0])}, 1000)
		} else {
			setTimeout(function() {$("#swipe_left").text(aWords[currentSubjectId][0]+ " — "+aContext[1])
			$("#swipe_right").text(aWords[currentSubjectId][1]+ " — "+aContext[0])}, 1000)
		}
	}
	
	TweenMax.to($("#swipe_3").children(".statement_indications").children(".indications"), 0, {top:(screenHeight-95-headerHeight)/2-$("#swipe_3").children(".statement_indications").children(".indications").innerHeight()/2+headerHeight});
	TweenMax.to($("#swipe_3"), 0, {autoAlpha:0});

	TweenMax.to($("#swipe_3").children(".statement_indications").children(".indications"), 0, {left:(screenWidth)/2-$("#swipe_3").children(".statement_indications").children(".indications").innerWidth()/2});
		
	TweenMax.to($("#swipe_3").children(".statement_indications").children(".indications"), 0, {autoAlpha:0});
		
	//TweenMax.to($(".type"), 1, {autoAlpha:1, ease:Quad.easeOut, delay:delay});
	
	TweenMax.to($("#swipe_3"), 1, {autoAlpha:1, ease:Quad.easeOut,overwrite:true,delay:delay});
	TweenMax.to($(".tiny"), 0.6, {autoAlpha:0,ease:Quad.easeOut, delay:delay});
	TweenMax.to($("#swipe_left"), .5, {autoAlpha:0, ease:Quad.easeOut,delay:delay});
			TweenMax.to($("#swipe_right"), .5, {autoAlpha:0, ease:Quad.easeOut,delay:delay});
	
	delay+=.9;
	TweenMax.to($("#contour"), 0, {opacity:1});
	
	TweenMax.to($("#contour"), .7, {scale:1,ease:Quad.easeOut,delay:delay, transformOrigin:"center"});
	delay+=.5;
	TweenMax.to($("#swipe_3").children(".statement_indications"), 0, {autoAlpha:1, ease:Quad.easeOut,overwrite:true, delay:delay});
	
	TweenMax.to($("#swipe_3").children(".statement_indications").children(".indications"), .5, {autoAlpha:1, ease:Quad.easeOut,overwrite:true, delay:delay});
	delay+=.5
	
	TweenMax.to($(".okbutton"), .5, {autoAlpha:1, ease:Quad.easeOut,overwrite:true, delay:delay});
	delay+.5;
	aStatements = aStatementBack.slice();
	shuffle(aStatements);
}

var yourBiased = 0;
//var aSubjects = ["racism", "sexism", "homophobia", "ageism","religion","weight","handicap"];
//var currentSubjectId = -1;

var aSubjectTopics = []
if(lang == "en") {
	aSubjectTopics.push(["black people","white people"])
	aSubjectTopics.push(["women","men"])
	aSubjectTopics.push(["gay people","straight people"])
	aSubjectTopics.push(["old people","young people"])
	aSubjectTopics.push(["Muslims","Christians"])
	aSubjectTopics.push(["fat people","thin people"])
	aSubjectTopics.push(["disabled people","able-bodied people"])
} else if(lang == "fr") {
	aSubjectTopics.push(["noirs","blancs"])
	aSubjectTopics.push(["femmes","hommes"])
	aSubjectTopics.push(["homosexuels","hétérosexuels"])
	aSubjectTopics.push(["vieux","jeunes"])
	aSubjectTopics.push(["musulmans","chrétiens"])
	aSubjectTopics.push(["gros","minces"])
	aSubjectTopics.push(["handicapés","non handicapé"])
} else if(lang == "de") {

	aSubjectTopics.push(["Schwarzen", "Weißen"])
	aSubjectTopics.push(["Frauen", "Männern"])
	aSubjectTopics.push(["Homosexuellen", "Heterosexuellen"])
	aSubjectTopics.push(["alten Menschen", "jung"])
	aSubjectTopics.push(["Muslimen", "christentum"])
	aSubjectTopics.push(["dicken Menschen", "dünn"])
	aSubjectTopics.push(["Behinderten", "nichtbehindert"])
}
function conclusion(_time1, _time2) {
	var delay=0;
	var totalDiff = 0;
	var totalPercent = 0;
	if(!flip) {
		totalDiff = _time2 - _time1;
		if(Math.abs(totalDiff) > 2) {
			yourBiased = 1;
		} else if(Math.abs(totalDiff) < 2) {
			yourBiased = 2;
		}
	} else {
		totalDiff = _time1 - _time2;
		if(Math.abs(totalDiff) > 2) {
			yourBiased = 1;
		} else if(Math.abs(totalDiff) < 2) {
			yourBiased = 2;
		}
	}
	totalPercent = Math.round(totalDiff/1000/30*50);

	if(totalPercent > 50) {
		totalPercent = 50;
	} else if(totalPercent <0) {
		totalPercent = 0;
	}
	
	
	
	if(yourBiased == 0) {
		//totalPercent = -totalPercent
	}
	
	
	
	if(totalPercent < sliderValue) {
		yourBiased = 0
		console.log("less biased than you thought")
	} else {
		yourBiased = 1
		console.log("more biased than you thought")
	
	}
	var biasType = "";
	if(Math.abs(totalPercent - sliderValue) < 5) {
		console.log("about as biased")
		yourBiased = 2
		
		if(lang == "en") {
			if(sliderValue < 1) {
				biasType = "not at all "
			} else 	if(sliderValue < 5) {
				biasType = "not "
			} else if(sliderValue < 10) {
				biasType = "not "
			} else if(sliderValue > 40) {
				biasType = "very "
			} else if(sliderValue > 30) {
				biasType = "pretty "
			} else if(sliderValue >= 20) {
				biasType = "a little "
			} else {
				biasType = "not really ";
			}
		} else if(lang =="fr") {
			if(sliderValue < 1) {
				biasType = "pas du tout "
			} else 		if(sliderValue < 5) {
				biasType = "pas "
			} else if(sliderValue < 10) {
				biasType = "pas "
			} else if(sliderValue > 40) {
				biasType = "beaucoup "
			} else if(sliderValue > 30) {
				biasType = "pas mal "
			} else if(sliderValue >= 20) {
				biasType = "un peu "
			} else {
				biasType = "pas vraiment ";
			}
		} else if(lang =="de") {
			if(sliderValue < 1) {
				biasType = "überhaupt nicht "
			} else 		if(sliderValue < 5) {
				biasType = "nicht "
			} else if(sliderValue < 10) {
				biasType = "nicht "
			} else if(sliderValue > 40) {
				biasType = "sehr "
			} else if(sliderValue > 30) {
				biasType = "ziemlich "
			} else if(sliderValue >= 20) {
				biasType = "ein wenig "
			} else {
				biasType = "nicht wirklich ";
			}
		}
	}
	
	console.log("sliderValue = "+sliderValue)
	console.log("totalPercent = "+totalPercent)
	
	console.log("totalTime = "+totalDiff)
	

	
	var sConclusion = "";
	/*if(yourBiased == 1) {
		sConclusion= "<img src=images/icn_fail.png width='82' height='83'/><br><br><span style='font-family: \"MotifLight\", sans-serif;'>Your answers suggest an inherent preference for "+aSubjectTopics[currentSubjectId][1]+".</span><br><br>You were quicker (" +Math.ceil(totalDiff/1000)+ " seconds to be exact) in associating "+aSubjectTopics[currentSubjectId][0]+" with unpleasant words than to pleasant words." ;
	} else if(yourBiased == 2) {
		sConclusion="<img src=images/icn_fail.png width='82' height='83'/><br><br><span style='font-family: \"MotifLight\", sans-serif;'>Your answers suggests no inherent preference for "+aSubjectTopics[currentSubjectId][0] +" or "+aSubjectTopics[currentSubjectId][1]+".</span><br><br>You were about as quick in associating "+aSubjectTopics[currentSubjectId][1]+" with unpleasant and pleasant words."

	} else {
		//sConclusion="<img src=images/icn_fail.png width='82' height='83'/><br><br>Wow. You were much quicker (" +Math.ceil(totalDiff/1000)+ " seconds to be exact) in associating "+aSubjectTopics[currentSubjectId][1]+" with unpleasant words than to pleasant words.<br><br>Why do you think that is? This data suggests an inherent preference for "+aSubjectTopics[currentSubjectId][0]+"."
	
		sConclusion= "<img src=images/icn_fail.png width='82' height='83'/><br><br><span style='font-family: \"MotifLight\", sans-serif;'>Your answers suggest an inherent preference for "+aSubjectTopics[currentSubjectId][0]+".</span><br><br>You were quicker (" +Math.ceil(totalDiff/1000)+ " seconds to be exact) in associating "+aSubjectTopics[currentSubjectId][1]+" with unpleasant words than to pleasant words." 	}*/
	
		//sConclusion= "<img src=images/icn_fail.png width='82' height='83'/><br><br><span style='font-family: \"MotifLight\", sans-serif;font-size:22px;'>Your answers suggests an inherent preference for "+aSubjectTopics[currentSubjectId][1]+".</span><br><br>You were much quicker (" +Math.ceil(totalDiff/1000)+ " seconds to be exact) in associating "+aSubjectTopics[currentSubjectId][0]+" with unpleasant words than to pleasant words." ;
	
	if(yourBiased == 1) {
		sConclusion= getResult1(biasType) ;
	} else if(yourBiased == 2) {
		sConclusion= getResult3(biasType, sliderValue) 
//sConclusion= getResult1() ;	
	} else {
		//sConclusion="<img src=images/icn_fail.png width='82' height='83'/><br><br>Wow. You were much quicker (" +Math.ceil(totalDiff/1000)+ " seconds to be exact) in associating "+aSubjectTopics[currentSubjectId][1]+" with unpleasant words than to pleasant words.<br><br>Why do you think that is? This data suggests an inherent preference for "+aSubjectTopics[currentSubjectId][0]+"."
	
		sConclusion= getResult2(biasType) ;
	}
	//$("#swipe_4").children(".statement_indications").children(".indications").css("top", "-40")
	$("#swipe_4").children(".statement_indications").children(".indications").html(sConclusion);
	
	TweenMax.to($("#swipe_4").children(".statement_indications").children(".indications"), 0, {top:50});
	
	
	
	$("#swipe_4").css( "zIndex", 11 );
	$("#swipe_4").css( "display", "block" );
	TweenMax.to($("#swipe_4"), 0, {x:0, y:0});
	
//	$(".logo").css("display","block")
	TweenMax.to($(".logo"), 1, {autoAlpha:1, ease:Quad.easeOut,overwrite:true,delay:delay});
	
	TweenMax.to($("#swipe_4").children(".statement_indications").children(".indications"), 0, {left:(screenWidth)/2-$("#swipe_4").children(".statement_indications").children(".indications").innerWidth()/2});
	$("#swipe_4").children(".statement_indications").children(".indications").css("top", (screenHeight-95-headerHeight) / 2 - ($("#swipe_4").children(".statement_indications").children(".indications").innerHeight()+115)/2+headerHeight-20)
	
	//TweenMax.to($("#swipe_4").children(".statement_indications").children(".indications"), 0, {top:(screenWidth)/2-$("#swipe_4").children(".statement_indications").children(".indications").innerWidth()/2});
	
	
	TweenMax.to($("#swipe_4").children(".statement_indications").children(".indications"), 0, {autoAlpha:0});
		
	
	TweenMax.to($("#swipe_4"), 1, {autoAlpha:1, ease:Quad.easeOut,overwrite:true,delay:delay});
	TweenMax.to($(".tiny"), 0.6, {autoAlpha:0,ease:Quad.easeOut, delay:delay});
	TweenMax.to($("#swipe_left"), .6, {autoAlpha:0, ease:Quad.easeOut,delay:delay});
			TweenMax.to($("#swipe_right"), .6, {autoAlpha:0, ease:Quad.easeOut,delay:delay});
	delay+=.9;
	TweenMax.to($("#contour"), 0, {opacity:1});
	
	TweenMax.to($("#contour"), .7, {scale:1,ease:Quad.easeOut,delay:delay, transformOrigin:"center"});
	delay+=.5;
	//$("#swipe_4").children(".statement_indications")
	TweenMax.to($("#swipe_4").children(".statement_indications"),0, {autoAlpha:1, ease:Quad.easeOut,overwrite:true, delay:0});
	TweenMax.to($("#swipe_4").children(".statement_indications").children(".indications"), .5, {autoAlpha:1, ease:Quad.easeOut,overwrite:true, delay:delay});
	//
	
	TweenMax.to($("#slider_text"), 0.5, {autoAlpha:1,delay:delay,ease:Quad.easeOut,overwrite:true});
	
	TweenMax.to($(".ui-slider-track"), 0.5, {autoAlpha:1,delay:delay,ease:Quad.easeOut,overwrite:true});
	TweenMax.to($("#drop"), 0.5, {autoAlpha:1,delay:delay,ease:Quad.easeOut,overwrite:true});
	TweenMax.to($("#slider_result"), 0.5, {autoAlpha:1,delay:delay,ease:Quad.easeOut,overwrite:true});
	TweenMax.to($("#slider_guess"), 0.5, {autoAlpha:1,delay:delay,ease:Quad.easeOut,overwrite:true});
	
	
	//TweenMax.to($(".ui-slider-track"), 0, {top:$("#swipe_4").children(".statement_indications").children(".indications").innerHeight()+120});
	//TweenMax.to($("#slider_text"), 0, {top:$("#swipe_4").children(".statement_indications").children(".indications").innerHeight()+95});
	if(Math.abs(totalPercent - sliderValue) < 5) {
		
	   	if(sliderValue > totalPercent) {
			totalPercent -= 10
		} else if(sliderValue < totalPercent) {
			totalPercent +=10
		} else {
	   	if(totalPercent > -50 && totalPercent < 50) {
			totalPercent += 10	
		} else {
			totalPercent -= 10;
		}
		
}
	} 
	
	if(totalPercent < -50) {
		totalPercent = -50;
	}
	
	if(totalPercent > 50) {
		totalPercent = 50;
	}
	
	TweenMax.to($(".ui-slider-track"), 0, {top:$("#swipe_4").children(".statement_indications").children(".indications").innerHeight()+$("#swipe_4").children(".statement_indications").children(".indications").position().top+45});
	TweenMax.to($("#slider_text"), 0, {top:$("#swipe_4").children(".statement_indications").children(".indications").innerHeight()+$("#swipe_4").children(".statement_indications").children(".indications").position().top+20});
	//totalPercent = 0;
	TweenMax.to($("#drop"), 0, {top:$("#swipe_4").children(".statement_indications").children(".indications").offset().top + $("#swipe_4").children(".statement_indications").children(".indications").innerHeight()+139-36-39});
	TweenMax.to($("#drop"), 0, {left:$(".ui-slider-track").offset().left +$(".ui-slider-track").width()*(totalPercent/50)-27});
	
	
	
	
	TweenMax.to($("#slider_result"), 0, {top:$("#swipe_4").children(".statement_indications").children(".indications").offset().top+$("#swipe_4").children(".statement_indications").children(".indications").innerHeight()+139+55-36-39+5});
	TweenMax.to($("#slider_result"), 0, {left:$(".ui-slider-track").offset().left +$(".ui-slider-track").width()*(totalPercent/50)-27-12+50-3});
	
	TweenMax.to($("#slider_guess"), 0, {top:$("#swipe_4").children(".statement_indications").children(".indications").offset().top+$("#swipe_4").children(".statement_indications").children(".indications").innerHeight()+139+55-36-39+5});
	TweenMax.to($("#slider_guess"), 0, {left:$(".ui-slider-track").offset().left +$(".ui-slider-track").width()*(sliderValue/50)-27-12+50-3});
	$( ".ui-slider-handle" ).css( 'pointer-events', 'none' );
	$( ".ui-slider-track" ).css( 'pointer-events', 'none' );
	
	//
	
	//$("#drop").
	
	delay+=.5
	
	TweenMax.to($(".okbutton"), .5, {autoAlpha:1, ease:Quad.easeOut,overwrite:true, delay:delay});
}

function closeConclusion1(){
	var delay = 0;
	
	$( ".ui-slider-handle" ).css( 'pointer-events', 'auto' );
		$( ".ui-slider-track" ).css( 'pointer-events', 'auto' );
	
	TweenMax.to($("#swipe_4").children(".okbutton"), 0.5, {autoAlpha:0,ease:Quad.easeOut,overwrite:true});
	delay+=.4;
	TweenMax.to($("#swipe_4").children(".statement_indications").children(".indications"), 0.5, {autoAlpha:0,ease:Quad.easeOut,overwrite:true, onComplete:swapConclusion});
	
	
	TweenMax.to($(".ui-slider-track"), 0.4, {autoAlpha:0,delay:delay,ease:Quad.easeOut,overwrite:true});
	TweenMax.to($("#slider_text"), 0.4, {autoAlpha:0,delay:delay,ease:Quad.easeOut,overwrite:true});
	TweenMax.to($("#slider_guess"), 0.4, {autoAlpha:0,delay:delay,ease:Quad.easeOut,overwrite:true});
	TweenMax.to($("#slider_result"), 0.4, {autoAlpha:0,delay:delay,ease:Quad.easeOut,overwrite:true});
	TweenMax.to($("#drop"), 0.4, {autoAlpha:0,delay:delay,ease:Quad.easeOut,overwrite:true});
	
	delay+=.6
	//TweenMax.to($("#swipe_2"), 0.5, {autoAlpha:0,ease:Quad.easeOut,overwrite:true});
	//TweenMax.to($("#contour"), 0.3, {scale:1.3,ease:Quad.easeOut,overwrite:true,transformOrigin:"center"});
	TweenMax.to($("#swipe_4").children(".statement_indications").children(".indications"), 0.6, {autoAlpha:1,ease:Quad.easeOut,delay:delay});
	
	
	//
}
//console.log()
function swapConclusion() {
	var sConclusion = getConclusion();
	$("#swipe_4").children(".statement_indications").children(".indications").html(sConclusion);
	
	setTimeout(setY, 10)
}

function setY() {
	$("#swipe_4").children(".statement_indications").children(".indications").css("top", (screenHeight-headerHeight) / 2 - ($("#swipe_4").children(".statement_indications").children(".indications").innerHeight())/2+headerHeight)
}

function showHowthisworks() {
	
}

function closeConclusion() {
	var delay = 0;
	$(".logo").css( "zIndex", 15 );
	
	$("#swipe_2").css( "zIndex", 16 );
	$(".type").css( "zIndex", 17 );
	$(".introtext").css( "zIndex", 18 );
	$("#icon_"+aSubjects[currentSubjectId]).css( "zIndex", 10009 );
	
	
	TweenMax.to($(".okbutton"), 0.6, {autoAlpha:0,ease:Quad.easeOut,overwrite:true});
	//TweenMax.to($("#contour"), 0.6, {scale:1.06,ease:Quad.easeIn,overwrite:true,transformOrigin:"center"});
	TweenMax.to($("#icon_"+aSubjects[currentSubjectId]), .6, {autoAlpha:0, ease:Quad.easeOut, delay:delay});
	TweenMax.to($(".type"), .6, {autoAlpha:0, ease:Quad.easeOut, delay:delay, transformOrigin:"center"});
	
	TweenMax.to($("#swipe_4").children(".statement_indications"), 0.6, {autoAlpha:0,ease:Quad.easeOut,overwrite:true, onComplete:function() {
		$(".logo").css("opacity",1);
	}});
	TweenMax.to($(".ui-slider-track"), 0.6, {autoAlpha:0,delay:delay,ease:Quad.easeOut,overwrite:true});
	TweenMax.to($("#slider_text"), 0.6, {autoAlpha:0,delay:delay,ease:Quad.easeOut,overwrite:true});
	TweenMax.to($("#slider_guess"), 0.6, {autoAlpha:0,delay:delay,ease:Quad.easeOut,overwrite:true});
	TweenMax.to($("#slider_result"), 0.6, {autoAlpha:0,delay:delay,ease:Quad.easeOut,overwrite:true});
	TweenMax.to($("#drop"), 0.6, {autoAlpha:0,delay:delay,ease:Quad.easeOut,overwrite:true});
	
	delay+=.6
	//TweenMax.to($("#swipe_2"), 0.5, {autoAlpha:0,ease:Quad.easeOut,overwrite:true});
	//TweenMax.to($("#contour"), 0.3, {scale:1.3,ease:Quad.easeOut,overwrite:true,transformOrigin:"center"});
	setTimeout(function() {$(".logo").css("display","block")},delay*1000) ;
	
	TweenMax.to($("#swipe_4"), 0.5, {x:0, y:-$(window).height(), ease:Expo.easeIn,overwrite:true, delay:delay});
	delay+=1;
	TweenMax.to($(".introtext"), 1, {autoAlpha:0, ease:Quad.easeOut,delay:delay, onComplete:resetEverything});
	
	//
}
var firstTime = true
function resetEverything() {
	$(".ui-slider-handle").attr("aria-valuenow", "0");
	$(".ui-slider-handle").css("left", "50%");
	warmupCounter = 0;
	aSubjects[currentSubjectId] = null;
	sliderValue = 0;
	console.log("reset everything")
	if(Math.random() > .5) {
		flip = true;
	} else {
		flip = false;
	}
	aTotalTime[0] = 0
	aTotalTime[1] = 0
	TweenMax.to($(".type"), 0, {top:"50%"});
	TweenMax.to($(".introtext"), 0, {y:0});
	$("#swipe_2").css("display", "none")
	$("#swipe_3").css("display", "none")
	TweenMax.to($(".type"), 0, {y:0, autoAlpha:0,ease:Expo.easeIn,overwrite:true, delay:0});
	
	TweenMax.to($("#swipe_2"), 0, {y:0, autoAlpha:0,ease:Expo.easeIn,overwrite:true, delay:0});
	TweenMax.to($("#swipe_3"), 0, {y:0,autoAlpha:0, ease:Expo.easeIn,overwrite:true, delay:0});
	currentSession =-1;
	aAnswers = new Array();
	aStatements = aStatementBack.slice();
	shuffle(aStatements);
	globalStatementId = 0;
	currentParentId = 0;
	currentId = 0;
	yourBiased = 0;
	randomSubject();
}

function startTimer() {
}

function setDragStart(e) {
	dragStartPos = $("#swipe_"+currentSwipe).position().left;
	console.log("start "+$(".swipe"));
}

function checkDragDistance(e) {
	setTimeout(completeCheckDragDistance,200);
} 

function completeCheckDragDistance() {
	console.log(dragStartPos);
	var dragEndPos = $("#swipe_"+currentSwipe).position().left;
	var dragDistance = dragStartPos - dragEndPos;
	console.log("distance "+dragDistance);
	if(Math.abs(dragDistance) < $(window).width()/1.8) {
		console.log("resetPosition");
		TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:0, y:0, ease:Back.easeOut});
		//$(".swipe").text("end drag 0");
	} else {
		if($("#swipe_"+currentSwipe).position().left > screenWidth/2) {
			likeIt();
		} else {
			unlikeIt();
		}
	}
}

function setOnClick() {

}

function likeIt(parentid) {
	console.log("like it")
	console.log(currentSession +" - " + flip + " - "+aStatements[currentParentId].type)
	if(!flip) {
		if(currentSession == 0) {
			if(aStatements[currentParentId].type == "pleasant" || aStatements[currentParentId].type == "white") {
				TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:$("#swipe_"+currentSwipe).width(), y:0, ease:Expo.easeOut,overwrite:true, onComplete:resetSlidePos});
				aStatements.shift();
				saveAnswer(currentParentId, 1);
				resetSlideValues();
				setNextStatement();
			} else {
				TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:0, y:0, ease:Expo.easeOut,overwrite:true});
				console.log("error");
				swipeError(currentParentId, 1)
			}
			//warmup
		} else if(currentSession == -1) {
			if(aStatements[currentParentId].type == "pleasant" || aStatements[currentParentId].type == "white") {
				TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:$("#swipe_"+currentSwipe).width(), y:0, ease:Expo.easeOut,overwrite:true, onComplete:resetSlidePos});
				aStatements.shift();
				//saveAnswer(currentParentId, 1);
				resetSlideValues();
				setNextStatement();
			} else {
				TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:0, y:0, ease:Expo.easeOut,overwrite:true});
				console.log("error");
				swipeError(currentParentId, 1)
			}
		}
		else if(currentSession == 1)  {
			if(aStatements[currentParentId].type == "negative" || aStatements[currentParentId].type == "white") {
				TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:$("#swipe_"+currentSwipe).width(), y:0, ease:Expo.easeOut,overwrite:true, onComplete:resetSlidePos});
				aStatements.shift();
				saveAnswer(currentParentId, 1);
				resetSlideValues();
				setNextStatement();
			} else {
				TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:0, y:0, ease:Expo.easeOut,overwrite:true});
				console.log("error");
				swipeError(currentParentId, 1)
			}
		}
	} else {
		if(currentSession == 0) {
			if(aStatements[currentParentId].type == "negative" || aStatements[currentParentId].type == "white") {
				TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:$("#swipe_"+currentSwipe).width(), y:0, ease:Expo.easeOut,overwrite:true, onComplete:resetSlidePos});
				aStatements.shift();
				saveAnswer(currentParentId, 1);
				resetSlideValues();
				setNextStatement();
			} else {
				TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:0, y:0, ease:Expo.easeOut,overwrite:true});
				console.log("error");
				swipeError(currentParentId, 1)
			}
			//warmup
		} else if(currentSession == -1) {
			if(aStatements[currentParentId].type == "negative" || aStatements[currentParentId].type == "white") {
				TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:$("#swipe_"+currentSwipe).width(), y:0, ease:Expo.easeOut,overwrite:true, onComplete:resetSlidePos});
				aStatements.shift();
				//saveAnswer(currentParentId, 1);
				resetSlideValues();
				setNextStatement();
			} else {
				TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:0, y:0, ease:Expo.easeOut,overwrite:true});
				console.log("error");
				swipeError(currentParentId, 1)
			}
		}
		else if(currentSession == 1)  {
			
			if(aStatements[currentParentId].type == "pleasant" || aStatements[currentParentId].type == "white") {
				TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:$("#swipe_"+currentSwipe).width(), y:0, ease:Expo.easeOut,overwrite:true, onComplete:resetSlidePos});
				aStatements.shift();
				saveAnswer(currentParentId, 1);
				resetSlideValues();
				setNextStatement();
			} else {
				TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:0, y:0, ease:Expo.easeOut,overwrite:true});
				console.log("error");
				swipeError(currentParentId, 1)
			}
		}
	}
}

function swipeError(parentId, answer) {
	console.log("swipe error")
	TweenMax.to($("#fail"), .2, {autoAlpha:1, ease:Quad.easeOut,overwrite:true});
	
	TweenMax.to($("#fail"), .5, {autoAlpha:0, ease:Quad.easeOut, delay:.8});
	var oAnswer = new Object();
	oAnswer.parentId = parentId;
	oAnswer.answer = answer;
	oAnswer.time = 1000;
	aAnswers.push(oAnswer);
}

function unlikeIt(parentid) {
	console.log("unlike it")
	console.log(currentSession +" - " + flip)
	
	if(!flip) {
	
		if(currentSession == 0) {
			if(aStatements[currentParentId].type == "negative" || aStatements[currentParentId].type == "black") {
				TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:-$("#swipe_"+currentSwipe).width(), y:0, ease:Expo.easeOut,overwrite:true, onComplete:resetSlidePos});
				aStatements.shift();
				saveAnswer(currentParentId, 0);
				resetSlideValues();
				setNextStatement();
			} else {
				TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:0, y:0, ease:Expo.easeOut,overwrite:true});
				console.log("error");
				swipeError(currentParentId, 0)
			}
			//warmup
		} else if(currentSession == -1) {
			if(aStatements[currentParentId].type == "negative" || aStatements[currentParentId].type == "black") {
				TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:-$("#swipe_"+currentSwipe).width(), y:0, ease:Expo.easeOut,overwrite:true, onComplete:resetSlidePos});
				aStatements.shift();
				//saveAnswer(currentParentId, 0);
				resetSlideValues();
				setNextStatement();
			} else {
				TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:0, y:0, ease:Expo.easeOut,overwrite:true});
				console.log("error");
				swipeError(currentParentId, 0)
			}
		} else {
			if(aStatements[currentParentId].type == "pleasant" || aStatements[currentParentId].type == "black") {
				TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:-$("#swipe_"+currentSwipe).width(), y:0, ease:Expo.easeOut,overwrite:true, onComplete:resetSlidePos});
				aStatements.shift();
				saveAnswer(currentParentId, 0);
				resetSlideValues();
				setNextStatement();
			} else {
				TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:0, y:0, ease:Expo.easeOut,overwrite:true});
				console.log("error");
				swipeError(currentParentId, 0)
			}
		}
	} else {
		
		if(currentSession == 0) {
			
			if(aStatements[currentParentId].type == "pleasant" || aStatements[currentParentId].type == "black") {
				TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:-$("#swipe_"+currentSwipe).width(), y:0, ease:Expo.easeOut,overwrite:true, onComplete:resetSlidePos});
				aStatements.shift();
				saveAnswer(currentParentId, 0);
				resetSlideValues();
				setNextStatement();
			} else {
				TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:0, y:0, ease:Expo.easeOut,overwrite:true});
				console.log("error");
				swipeError(currentParentId, 0)
			}
			//warmup
		} else if(currentSession == -1) {
			
			if(aStatements[currentParentId].type == "pleasant" || aStatements[currentParentId].type == "black") {
				TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:-$("#swipe_"+currentSwipe).width(), y:0, ease:Expo.easeOut,overwrite:true, onComplete:resetSlidePos});
				aStatements.shift();
				//saveAnswer(currentParentId, 0);
				resetSlideValues();
				setNextStatement();
			} else {
				TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:0, y:0, ease:Expo.easeOut,overwrite:true});
				console.log("error");
				swipeError(currentParentId, 0)
			}
		} else {
			if(aStatements[currentParentId].type == "negative" || aStatements[currentParentId].type == "black") {
				TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:-$("#swipe_"+currentSwipe).width(), y:0, ease:Expo.easeOut,overwrite:true, onComplete:resetSlidePos});
				aStatements.shift();
				saveAnswer(currentParentId, 0);
				resetSlideValues();
				setNextStatement();
			} else {
				TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:0, y:0, ease:Expo.easeOut,overwrite:true});
				console.log("error");
				swipeError(currentParentId, 0)
			}
		}
	}
}

function saveAnswer(parentId, answer) {
	var oAnswer = new Object();
	d = new Date();
	oAnswer.parentId = parentId;
	oAnswer.answer = answer;
	oAnswer.time = d.getTime()-statementShowTime;
	aAnswers.push(oAnswer);
	console.log(aAnswers)
}

var lastType = "";
var aTotalTime = [];

var warmupCounter = 0;

function placeStatement() {
	if($("#swipe_"+currentSwipe).children(".statement").children(".text").html().indexOf(".png") == -1) {
		
	
			TweenMax.to($("#swipe_"+currentSwipe).children(".statement").children(".text"), 0, {top:(screenHeight-88)/2-$("#swipe_"+currentSwipe).children(".statement").children(".text").innerHeight()/2-20+20, ease:Quad.easeOut,delay:0});
			TweenMax.to($("#swipe_"+currentSwipe).children(".statement").children(".text"), .3, {top:(screenHeight-88)/2-$("#swipe_"+currentSwipe).children(".statement").children(".text").innerHeight()/2+20, opacity:1, ease:Quad.easeOut,delay:.1});
		} else {
				TweenMax.to($("#swipe_"+currentSwipe).children(".statement").children(".text"), 0, {top:(screenHeight-88)/2-116/2-20+20, ease:Quad.easeOut,delay:0});
			TweenMax.to($("#swipe_"+currentSwipe).children(".statement").children(".text"), .3, {top:(screenHeight-88)/2-116/2+20, opacity:1, ease:Quad.easeOut,delay:.1});
	
		}
}


function setNextStatement() {
	console.log("setNextStatement ")
	if(currentSession == -1) {
		if(warmupCounter < 6) {
			while((aStatements[0].type != "white" && aStatements[0].type != "black") || (lastType == "white" && aStatements[0].type == "white") || (lastType == "black" && aStatements[0].type == "black")) {
				shuffle(aStatements);
			}
			lastType = aStatements[0].type;
			var sNumber = "";
			if(globalStatementId+1 < 10) {
				sNumber = "0"+(globalStatementId+1)
			} else {
				sNumber = globalStatementId+1
			}

			$("#swipe_"+currentSwipe).children(".statement").children(".number").text(sNumber+"/"+"06");
			$("#swipe_"+currentSwipe).children(".statement").children(".text").html(aStatements[0].word);
			TweenMax.to($("#swipe_"+currentSwipe).children(".statement"), 0, {x:$(window).width()/2-$("#swipe_"+currentSwipe).children(".statement").innerWidth()/2});
			TweenMax.to($("#swipe_"+currentSwipe).children(".statement").children(".text"), 0, {x:$(window).width()/2-$("#swipe_"+currentSwipe).children(".statement").children(".text").innerWidth()/2});

			TweenMax.to($("#swipe_"+currentSwipe).children(".statement"), 0, {opacity:1, ease:Quad.easeOut});
			TweenMax.to($("#swipe_"+currentSwipe).children(".statement").children(".number"), 0, {ease:Quad.easeOut,delay:0});
			TweenMax.to($("#swipe_"+currentSwipe).children(".statement").children(".separator"), 0, {height:0, ease:Quad.easeOut,delay:0});
			setTimeout(placeStatement,50)
			//TweenMax.to($("#swipe_"+currentSwipe).children(".statement").children(".text"), 0, {top:(screenHeight-88)/2-17-20, ease:Quad.easeOut,delay:0});

			TweenMax.to($("#swipe_"+currentSwipe).children(".statement").children(".number"), .5, {y:0,opacity:1, ease:Quad.easeOut,delay:0});
			TweenMax.to($("#swipe_"+currentSwipe).children(".statement").children(".separator"), .25, {height:12,opacity:1, ease:Quad.easeOut,delay:.1});
			//TweenMax.to($("#swipe_"+currentSwipe).children(".statement").children(".text"), .3, {top:(screenHeight-88)/2-17, opacity:1, ease:Quad.easeOut,delay:.1});
			//console.log("screenHeight "+((screenHeight-88)/2))

			globalStatementId++;
			//d = new Date();
			//statementShowTime = d.getTime();
			//console.log("statementShowTime "+statementShowTime)
		
			warmupCounter++;
		} else {
			resetSession2();
			//currentSession = 0;
		}
	} else {
		if(aStatements.length > 0) {
			if(aStatements.length > 1) {
				while(((lastType == "black" || lastType == "white") && (aStatements[0].type == "white" || aStatements[0].type == "black")) || ((lastType == "negative" || lastType == "pleasant") && (aStatements[0].type == "negative" || aStatements[0].type == "pleasant"))) {
					shuffle(aStatements);
				}
			}
			lastType = aStatements[0].type;

			var sNumber = "";
			if(globalStatementId+1 < 10) {
				sNumber = "0"+(globalStatementId+1)
			} else {
				sNumber = globalStatementId+1
			}

			$("#swipe_"+currentSwipe).children(".statement").children(".number").text(sNumber+"/"+40);
			$("#swipe_"+currentSwipe).children(".statement").children(".text").html(aStatements[0].word);
			TweenMax.to($("#swipe_"+currentSwipe).children(".statement"), 0, {x:$(window).width()/2-$("#swipe_"+currentSwipe).children(".statement").innerWidth()/2});
			TweenMax.to($("#swipe_"+currentSwipe).children(".statement").children(".text"), 0, {x:$(window).width()/2-$("#swipe_"+currentSwipe).children(".statement").children(".text").innerWidth()/2});

			TweenMax.to($("#swipe_"+currentSwipe).children(".statement"), 0, {opacity:1, ease:Quad.easeOut});
			TweenMax.to($("#swipe_"+currentSwipe).children(".statement").children(".number"), 0, {ease:Quad.easeOut,delay:0});
			TweenMax.to($("#swipe_"+currentSwipe).children(".statement").children(".separator"), 0, {height:0, ease:Quad.easeOut,delay:0});
			//TweenMax.to($("#swipe_"+currentSwipe).children(".statement").children(".text"), 0, {top:(screenHeight-88)/2-17-20, ease:Quad.easeOut,delay:0});
			setTimeout(placeStatement,50)
			TweenMax.to($("#swipe_"+currentSwipe).children(".statement").children(".number"), .5, {y:0,opacity:1, ease:Quad.easeOut,delay:0});
			TweenMax.to($("#swipe_"+currentSwipe).children(".statement").children(".separator"), .25, {height:12,opacity:1, ease:Quad.easeOut,delay:.1});
			//TweenMax.to($("#swipe_"+currentSwipe).children(".statement").children(".text"), .3, {top:(screenHeight-88)/2-17, opacity:1, ease:Quad.easeOut,delay:.1});
			console.log("screenHeight "+((screenHeight-88)/2))

			globalStatementId++;
			d = new Date();
			statementShowTime = d.getTime();
			console.log("statementShowTime "+statementShowTime)
		} else {
			//TweenMax.to($("#swipe_left"), .5, {autoAlpha:0, ease:Quad.easeOut,delay:0});
			//TweenMax.to($("#swipe_right"), .5, {autoAlpha:0, ease:Quad.easeOut,delay:0});
		
			console.log("done")	;
			var totalTime = 0;
			for(var i=0; i<aAnswers.length; i++) {
				totalTime += aAnswers[i].time;
			}

			if(currentSession == 0) {
				resetSession();
			} 
			aTotalTime[currentSession] = totalTime;
			console.log("totalTime1 "+aTotalTime[0])	;
			console.log("totalTime2 "+aTotalTime[1])	;
			if(currentSession == 1) {
				conclusion(aTotalTime[0], aTotalTime[1])
				//alert(aTotalTime[0] + " -- " + aTotalTime[1])
			}

			currentSession = 1;
		}
	}
}

function resetSession() {
	globalStatementId =0;
	aAnswers = new Array();
	console.log("resetSession "+currentSession)
	$("#swipe_3").css("display","block");
	$("#swipe_3").css( "zIndex", 10 );
	$("#contour").css( "zIndex", 10005 );
	
	init2();
}

function resetSession2() {
	currentSession = 0;
	globalStatementId =0;
	aAnswers = new Array();
	console.log("resetSession2 "+currentSession)
	$("#swipe_3").css("display","block");
	$("#swipe_3").css( "zIndex", 10 );
	$("#contour").css( "zIndex", 10005 );
	
	init3();
}

function resetSlideValues() {
	if(currentSwipe == 0) {
		currentSwipe = 1;
		otherSwipe = 0;
	} else {
		currentSwipe = 0;
		otherSwipe = 1;
	}
}

function resetSlidePos() {

	TweenMax.to("#swipe_"+otherSwipe, 0, {x:0, y:0, overwrite:true});
	$("#swipe_"+otherSwipe).children(".statement").css( "opacity", 0 );
	$("#swipe_"+otherSwipe).children(".statement").children(".number").css( "opacity", 0 );
	$("#swipe_"+otherSwipe).children(".statement").children(".separator").css( "opacity", 0 );
	$("#swipe_"+otherSwipe).children(".statement").children(".text").css( "opacity", 0 );

	$("#swipe_"+otherSwipe).css( "zIndex", 0 );
	$("#swipe_"+currentSwipe).css( "zIndex", 10 );

}

function shuffle(a) {
	var j, x, i;
	for (i = a.length; i; i--) {
		j = Math.floor(Math.random() * i);
		x = a[i - 1];
		a[i - 1] = a[j];
		a[j] = x;
	}
}