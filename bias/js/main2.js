var aColors = [];
var dragStartPos = 0;
var origx = 0;
var origy = 0;
var globalStatementId = 0;
var currentParentId = 0;
var currentId = 0;
var currentSwipe = 0;
var otherSwipe = 1;

var screenWidth = 0;
var statementShowTime =0;
var statementAnswerTime = 0;
var draggers = new Array();
var d = new Date();
var aAnswers = [];
var currentSession =0;

var aStatementBack = [];
var aStatements = [];


var aTypes = new Array();
aTypes.push({name:"Black people", id:0});
aTypes.push({name:"Women", id:1});
aTypes.push({name:"Homosexuals", id:2});
aTypes.push({name:"Old people", id:3});
aTypes.push({name:"Religions", id:4});
aTypes.push({name:"Fat people", id:5});
aTypes.push({name:"People with disabilities", id:6});
//shuffle(aTypes);	

var aSubjects = ["racism", "sexism", "homophobia", "ageism","religion","weight","handicap"];
var currentSubjectId = -1;

$( document ).ready(function() {
	document.ontouchmove = function(e){ e.preventDefault(); }
	screenWidth = $(window).width();
	$("#contour").css("width", screenWidth-20);
	$("#contour").css("height", $(window).height()-20);
	TweenMax.to($("#contour"), 0, {scale:1.06,ease:Quad.easeIn,overwrite:true,transformOrigin:"center"});
	
	init();
});

function initIntro() {
	
	//setTimeout(randomSubject, delay*1000)
}

function init() {
	console.log("init")
	var delay = 1;
	$(".logogif").attr("src","images/logo3.gif");
	//TweenMax.to($(".introtext"), 0, {top:"50%", ease:Quad.easeOut,delay:delay});
	
	TweenMax.to($(".logoimage"), 2, {alpha:1, ease:Quad.easeOut,delay:delay});
	delay+=2.5;
	TweenMax.to($(".logoimage"), 1, {alpha:0, ease:Expo.easeInOut,delay:delay});
	delay+=.5
	TweenMax.to($("#contour"), 1, {scale:1,ease:Expo.easeOut,delay:delay, transformOrigin:"center"});
	
	delay+=1;
	TweenMax.to($(".introtext"), 1, {alpha:1, ease:Quad.easeOut,delay:delay,overwrite:true});
	//delay+=1;
	//TweenMax.to($(".introtext"), .7, {top:90, ease:Expo.easeOut,delay:delay});
	
	delay-=.5;
	
	
	
	setTimeout(randomSubject, delay*1000)
	delay+=7
	
	//setTimeout(showInstructions, delay*1000)
	/*TweenMax.to($("#swipe_2"), 0.5, {x:0, y:-$(window).height(), ease:Expo.easeIn,overwrite:true});

	window.onkeyup = function(e) {
		var key = e.keyCode ? e.keyCode : e.which;
		if (key == 37) {
			console.log("left");
		  unlikeIt(0);
		}else if (key == 39) {
			console.log("right");
		   likeIt(1);					}
	}
	draggers[0] = Draggable.create("#swipe_0", {type:"x", edgeResistance:0.65, minimumMovement:0, throwProps:true, onDragStart:setDragStart, onDragEnd:checkDragDistance, onClick:setOnClick});
	draggers[1] = Draggable.create("#swipe_1", {type:"x", edgeResistance:0.65, minimumMovement:0, throwProps:true, onDragStart:setDragStart, onDragEnd:checkDragDistance, onClick:setOnClick});
	setNextStatement();
	startTimer();*/
	initIcons();
}
var currentIcon = "";
function iconDone(_icon) {
	console.log("icon done")
	var delay =2.5 ;
	TweenMax.to($(".introtext"), .8, {y:-500, alpha:0, ease:Expo.easeInOut, delay:delay});
	TweenMax.to($("#icon_"+_icon), .8, {y:-500, alpha:0, ease:Expo.easeInOut, delay:delay+0.1, transformOrigin:"94px 94px"});
	
	delay+=.1;
	TweenMax.to($(".type"), 1, {top:10, ease:Expo.easeInOut, delay:delay, transformOrigin:"center"});
	delay+=.5;
	setTimeout(showInstructions, delay*1000)
	//delay+=.2
	$("#icon_"+_icon).css( "zIndex", 10005 );
	$(".type").css( "zIndex", 10006 );
	
}

function showInstructions() {
	
	console.log("show instructions");
	//$("#swipe_2").child("statement_indications").child("indications").left = 
	$("#swipe_2").css("display", "block")
	
	TweenMax.to($("#swipe_2"), 0, {alpha:0});

	TweenMax.to($("#swipe_2").children(".statement_indications").children(".indications"), 0, {left:(screenWidth)/2-$("#swipe_2").children(".statement_indications").children(".indications").innerWidth()/2});
		
		
	TweenMax.to($("#swipe_2"), 1, {alpha:1, ease:Quad.easeOut,overwrite:true});

	TweenMax.to($("#swipe_2").children(".statement_indications"), 0.6, {alpha:1,delay:1,ease:Quad.easeOut,overwrite:true});
	TweenMax.to($(".okbutton"), .5, {alpha:1, ease:Quad.easeOut,overwrite:true, delay:1.6});
	
	TweenMax.to($(".logo"), 1, {alpha:0, ease:Quad.easeOut,delay:.5, onComplete:function(){$(".logo").css("display","none")}});
	
}

function randomSubject() {
	currentSubjectId = Math.round(Math.random()*(aSubjects.length-1));
	//currentSubjectId = 0;
	console.log(currentSubjectId)
	//currentSubjectId = 6
	
	loadPicto(aSubjects[currentSubjectId]);
	if(aSubjects[currentSubjectId] == "racism") {
		aStatements = aStatements_racism;
		aStatementBack = aStatementBack_racism
		
	} else if(aSubjects[currentSubjectId] == "sexism") {
		aStatements = aStatements_sexism; 
		aStatementBack = aStatementBack_sexism
		
	} else if(aSubjects[currentSubjectId] == "homophobia") {
		aStatements = aStatements_homophobia; 
		aStatementBack = aStatementBack_homophobia
		
	}else if(aSubjects[currentSubjectId] == "ageism") {
		aStatements = aStatements_ageism; 
		aStatementBack = aStatementBack_ageism
		
	}else if(aSubjects[currentSubjectId] == "religion") {
		aStatements = aStatements_religion; 
		aStatementBack = aStatementBack_religion
		
	}else if(aSubjects[currentSubjectId] == "weight") {
		aStatements = aStatements_weight; 
		aStatementBack = aStatementBack_weight
		
	}else if(aSubjects[currentSubjectId] == "weapons") {
		aStatements = aStatements_weapons; 
		aStatementBack = aStatementBack_weapons
		
	}else if(aSubjects[currentSubjectId] == "handicap") {
		aStatements = aStatements_handicap; 
		aStatementBack = aStatementBack_handicap
		
	}
	console.log("random "+aSubjects[currentSubjectId])
	
	//$(".type").css("top","50%");
	
	$(".type").text(aTypes[currentSubjectId].name);
	TweenMax.to($(".introtext"), 1, {alpha:1, ease:Quad.easeOut,delay:0});
	
	TweenMax.to($(".type"), 1, {alpha:1, ease:Quad.easeOut, overwrite:true, delay:2.4});
	
}

function closeInstructions1() {
	//okbutton
	var delay = 0;
	TweenMax.to($(".okbutton"), 0.6, {alpha:0,ease:Quad.easeOut,overwrite:true});
	TweenMax.to($("#contour"), 0.6, {scale:1.06,ease:Quad.easeIn,overwrite:true,transformOrigin:"center"});
	TweenMax.to($("#swipe_2").children(".statement_indications"), 0.6, {alpha:0,ease:Quad.easeOut,overwrite:true});
	delay+=.6
	//TweenMax.to($("#swipe_2"), 0.5, {autoAlpha:0,ease:Quad.easeOut,overwrite:true});
	//TweenMax.to($("#contour"), 0.3, {scale:1.3,ease:Quad.easeOut,overwrite:true,transformOrigin:"center"});
	
	TweenMax.to($("#swipe_2"), 0.5, {x:0, y:-$(window).height(), ease:Expo.easeIn,overwrite:true, delay:delay});
	window.onkeyup = function(e) {
		var key = e.keyCode ? e.keyCode : e.which;
		if (key == 37) {
			console.log("left");
		  unlikeIt(0);
		}else if (key == 39) {
			console.log("right");
		   likeIt(1);					}
	}
	draggers[0] = Draggable.create("#swipe_0", {type:"x", edgeResistance:0.65, minimumMovement:0, throwProps:true, onDragStart:setDragStart, onDragEnd:checkDragDistance, onClick:setOnClick});
	draggers[1] = Draggable.create("#swipe_1", {type:"x", edgeResistance:0.65, minimumMovement:0, throwProps:true, onDragStart:setDragStart, onDragEnd:checkDragDistance, onClick:setOnClick});
	setNextStatement();
	startTimer();
}

function closeInstructions2() {
	//okbutton
	var delay = 0;
	TweenMax.to($(".okbutton"), 0.6, {alpha:0,ease:Quad.easeOut,overwrite:true});
	TweenMax.to($("#contour"), 0.6, {scale:1.06,ease:Quad.easeIn,overwrite:true,transformOrigin:"center"});
	TweenMax.to($("#swipe_3").children(".statement_indications"), 0.6, {alpha:0,ease:Quad.easeOut,overwrite:true});
	delay+=.6
	//TweenMax.to($("#swipe_2"), 0.5, {autoAlpha:0,ease:Quad.easeOut,overwrite:true});
	//TweenMax.to($("#contour"), 0.3, {scale:1.3,ease:Quad.easeOut,overwrite:true,transformOrigin:"center"});
	
	TweenMax.to($("#swipe_3"), 0.5, {x:0, y:-$(window).height(), ease:Expo.easeIn,overwrite:true, delay:delay});
	setNextStatement();
	
	//startTimer();
}

function init2() {
	console.log("init2")
	var delay=0;
	$("#swipe_3").css("display", "block")
	
	TweenMax.to($("#swipe_3"), 0, {alpha:0});

	TweenMax.to($("#swipe_3").children(".statement_indications").children(".indications"), 0, {left:(screenWidth)/2-$("#swipe_3").children(".statement_indications").children(".indications").innerWidth()/2});
		
	TweenMax.to($("#swipe_3").children(".statement_indications").children(".indications"), 0, {alpha:0});
		
	
	TweenMax.to($("#swipe_3"), 1, {alpha:1, ease:Quad.easeOut,overwrite:true,delay:delay});
	delay+=.9;
	
	TweenMax.to($("#contour"), .7, {scale:1,ease:Quad.easeOut,delay:delay, transformOrigin:"center"});
	delay+=.5;
	TweenMax.to($("#swipe_3").children(".statement_indications"), 0, {alpha:1, ease:Quad.easeOut,overwrite:true, delay:delay});
	
	TweenMax.to($("#swipe_3").children(".statement_indications").children(".indications"), .5, {alpha:1, ease:Quad.easeOut,overwrite:true, delay:delay});
	delay+=.5
	
	TweenMax.to($(".okbutton"), .5, {alpha:1, ease:Quad.easeOut,overwrite:true, delay:delay});
delay+.5;
	aStatements = aStatementBack.slice();
	shuffle(aStatements);
	//setTimeout(setNextStatement, delay*1000);
}

function conclusion(_time1, _time2) {
	var delay=0;
	$("#swipe_4").css( "zIndex", 11 );
	$("#swipe_4").css( "display", "block" );
	TweenMax.to($("#swipe_4"), 0, {x:0, y:0});
	
//	$(".logo").css("display","block")
	TweenMax.to($(".logo"), 1, {autoAlpha:1, ease:Quad.easeOut,overwrite:true,delay:delay});
	
	TweenMax.to($("#swipe_4").children(".statement_indications").children(".indications"), 0, {left:(screenWidth)/2-$("#swipe_4").children(".statement_indications").children(".indications").innerWidth()/2});
		
	TweenMax.to($("#swipe_4").children(".statement_indications").children(".indications"), 0, {alpha:0});
		
	
	TweenMax.to($("#swipe_4"), 1, {autoAlpha:1, ease:Quad.easeOut,overwrite:true,delay:delay});
	delay+=.9;
	
	TweenMax.to($("#contour"), .7, {scale:1,ease:Quad.easeOut,delay:delay, transformOrigin:"center"});
	delay+=.5;
	//$("#swipe_4").children(".statement_indications")
	TweenMax.to($("#swipe_4").children(".statement_indications"),0, {alpha:1, ease:Quad.easeOut,overwrite:true, delay:0});
	TweenMax.to($("#swipe_4").children(".statement_indications").children(".indications"), .5, {alpha:1, ease:Quad.easeOut,overwrite:true, delay:delay});
	delay+=.5
	
	TweenMax.to($(".okbutton"), .5, {alpha:1, ease:Quad.easeOut,overwrite:true, delay:delay});
}

function closeConclusion() {
	var delay = 0;
	$(".logo").css( "zIndex", 15 );
	
	$("#swipe_2").css( "zIndex", 16 );
	$(".type").css( "zIndex", 17 );
	$(".introtext").css( "zIndex", 18 );
	$("#icon_"+aSubjects[currentSubjectId]).css( "zIndex", 10009 );
	
	
	TweenMax.to($(".okbutton"), 0.6, {alpha:0,ease:Quad.easeOut,overwrite:true});
	//TweenMax.to($("#contour"), 0.6, {scale:1.06,ease:Quad.easeIn,overwrite:true,transformOrigin:"center"});
	TweenMax.to($("#icon_"+aSubjects[currentSubjectId]), .6, {alpha:0, ease:Quad.easeOut, delay:delay});
	TweenMax.to($(".type"), .6, {alpha:0, ease:Quad.easeOut, delay:delay, transformOrigin:"center"});
	
	TweenMax.to($("#swipe_4").children(".statement_indications"), 0.6, {alpha:0,ease:Quad.easeOut,overwrite:true, onComplete:function() {
		$(".logo").css("opacity",1);
	}});
	delay+=.6
	//TweenMax.to($("#swipe_2"), 0.5, {autoAlpha:0,ease:Quad.easeOut,overwrite:true});
	//TweenMax.to($("#contour"), 0.3, {scale:1.3,ease:Quad.easeOut,overwrite:true,transformOrigin:"center"});
	setTimeout(function() {$(".logo").css("display","block")},delay*1000) ;
	
	TweenMax.to($("#swipe_4"), 0.5, {x:0, y:-$(window).height(), ease:Expo.easeIn,overwrite:true, delay:delay});
	delay+=1;
	TweenMax.to($(".introtext"), 1, {alpha:0, ease:Quad.easeOut,delay:delay, onComplete:resetEverything});
	
	//
}

function resetEverything() {
	TweenMax.to($(".type"), 0, {top:"50%"});
	TweenMax.to($(".introtext"), 0, {y:0});
	$("#swipe_2").css("display", "none")
	$("#swipe_3").css("display", "none")
	
	TweenMax.to($("#swipe_2"), 0, {y:0, alpha:0,ease:Expo.easeIn,overwrite:true, delay:0});
	TweenMax.to($("#swipe_3"), 0, {y:0,alpha:0, ease:Expo.easeIn,overwrite:true, delay:0});
	currentSession =0;
	aAnswers = new Array();
	aStatements = aStatementBack.slice();
	shuffle(aStatements);
	globalStatementId = 0;
	currentParentId = 0;
	currentId = 0;
	randomSubject();
}

function startTimer() {
}

function setDragStart(e) {
	dragStartPos = $("#swipe_"+currentSwipe).position().left;
	console.log("start "+$(".swipe"));
}

function checkDragDistance(e) {
	setTimeout(completeCheckDragDistance,200);
} 

function completeCheckDragDistance() {
	console.log(dragStartPos);
	var dragEndPos = $("#swipe_"+currentSwipe).position().left;
	var dragDistance = dragStartPos - dragEndPos;
	console.log("distance "+dragDistance);
	if(Math.abs(dragDistance) < $(window).width()/1.8) {
		console.log("resetPosition");
		TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:0, y:0, ease:Back.easeOut});
		//$(".swipe").text("end drag 0");
	} else {
		if($("#swipe_"+currentSwipe).position().left > $("window").width()/2) {
			likeIt();
		} else {
			unlikeIt();
		}
	}
}

function setOnClick() {

}

function likeIt(parentid) {
	if(currentSession == 0) {
		if(aStatements[currentParentId].type == "pleasant" || aStatements[currentParentId].type == "white") {
			TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:$("#swipe_"+currentSwipe).width(), y:0, ease:Expo.easeOut,overwrite:true, onComplete:resetSlidePos});
			aStatements.shift();
			saveAnswer(currentParentId, 1);
			resetSlideValues();
			setNextStatement();
		} else {
			TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:0, y:0, ease:Expo.easeOut,overwrite:true});
			console.log("error");
		}
	} else {
		if(aStatements[currentParentId].type == "negative" || aStatements[currentParentId].type == "white") {
			TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:$("#swipe_"+currentSwipe).width(), y:0, ease:Expo.easeOut,overwrite:true, onComplete:resetSlidePos});
			aStatements.shift();
			saveAnswer(currentParentId, 1);
			resetSlideValues();
			setNextStatement();
		} else {
			TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:0, y:0, ease:Expo.easeOut,overwrite:true});
			console.log("error");
		}
	}
}

function unlikeIt(parentid) {
	if(currentSession == 0) {
		if(aStatements[currentParentId].type == "negative" || aStatements[currentParentId].type == "black") {
			TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:-$("#swipe_"+currentSwipe).width(), y:0, ease:Expo.easeOut,overwrite:true, onComplete:resetSlidePos});
			aStatements.shift();
			saveAnswer(currentParentId, 0);
			resetSlideValues();
			setNextStatement();
		} else {
			TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:0, y:0, ease:Expo.easeOut,overwrite:true});
			console.log("error");
		}
	} else {
		if(aStatements[currentParentId].type == "pleasant" || aStatements[currentParentId].type == "black") {
			TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:-$("#swipe_"+currentSwipe).width(), y:0, ease:Expo.easeOut,overwrite:true, onComplete:resetSlidePos});
			aStatements.shift();
			saveAnswer(currentParentId, 0);
			resetSlideValues();
			setNextStatement();
		} else {
			TweenMax.to($("#swipe_"+currentSwipe), 0.5, {x:0, y:0, ease:Expo.easeOut,overwrite:true});
			console.log("error");
		}
	}
}

function saveAnswer(parentId, answer) {
	var oAnswer = new Object();
	d = new Date();
	oAnswer.parentId = parentId;
	oAnswer.answer = answer;
	oAnswer.time = d.getTime()-statementShowTime;
	aAnswers.push(oAnswer);
	console.log(aAnswers)
}

var lastType = "";
var aTotalTime = [];

function setNextStatement() {
	console.log("setNextStatement ")
	if(aStatements.length > 38) {
		if(aStatements.length > 1) {
			while(((lastType == "black" || lastType == "white") && (aStatements[0].type == "white" || aStatements[0].type == "black")) || ((lastType == "negative" || lastType == "pleasant") && (aStatements[0].type == "negative" || aStatements[0].type == "pleasant"))) {
				shuffle(aStatements);
			}
		}
		lastType = aStatements[0].type;
		$("#swipe_"+currentSwipe).children(".statement").children(".number").text(globalStatementId+1);
		$("#swipe_"+currentSwipe).children(".statement").children(".text").text(aStatements[0].word);
		TweenMax.to($("#swipe_"+currentSwipe).children(".statement"), 0, {x:$(window).width()/2-$("#swipe_"+currentSwipe).children(".statement").innerWidth()/2});
		TweenMax.to($("#swipe_"+currentSwipe).children(".statement").children(".text"), 0, {x:$(window).width()/2-$("#swipe_"+currentSwipe).children(".statement").children(".text").innerWidth()/2});

		TweenMax.to($("#swipe_"+currentSwipe).children(".statement"), 0, {opacity:1, ease:Quad.easeOut});
		TweenMax.to($("#swipe_"+currentSwipe).children(".statement").children(".number"), 0, {ease:Quad.easeOut,delay:0});
		TweenMax.to($("#swipe_"+currentSwipe).children(".statement").children(".separator"), 0, {height:0, ease:Quad.easeOut,delay:0});
		TweenMax.to($("#swipe_"+currentSwipe).children(".statement").children(".text"), 0, {y:20, ease:Quad.easeOut,delay:0});

		TweenMax.to($("#swipe_"+currentSwipe).children(".statement").children(".number"), .5, {y:0,opacity:1, ease:Quad.easeOut,delay:0});
		TweenMax.to($("#swipe_"+currentSwipe).children(".statement").children(".separator"), .25, {height:40,opacity:1, ease:Quad.easeOut,delay:.1});
		TweenMax.to($("#swipe_"+currentSwipe).children(".statement").children(".text"), .3, {y:0, opacity:1, ease:Quad.easeOut,delay:.1});


		globalStatementId++;
		d = new Date();
		statementShowTime = d.getTime();
		console.log("statementShowTime "+statementShowTime)
	} else {
		console.log("done")	;
		var totalTime = 0;
		for(var i=0; i<aAnswers.length; i++) {
			totalTime += aAnswers[i].time;
		}

		if(currentSession == 0) {
			resetSession();
		} 
		aTotalTime[currentSession] = totalTime;
		console.log("totalTime1 "+aTotalTime[0])	;
		console.log("totalTime2 "+aTotalTime[1])	;
		if(currentSession == 1) {
			conclusion(aTotalTime[0], aTotalTime[1])
			//alert(aTotalTime[0] + " -- " + aTotalTime[1])
		}

		currentSession = 1;
	}
}

function resetSession() {
	globalStatementId =0;
	aAnswers = new Array();
	console.log("resetSession "+currentSession)
	$("#swipe_3").css("display","block");
	$("#swipe_3").css( "zIndex", 10 );
	$("#contour").css( "zIndex", 10005 );
	
	init2();
}

function resetSlideValues() {
	if(currentSwipe == 0) {
		currentSwipe = 1;
		otherSwipe = 0;
	} else {
		currentSwipe = 0;
		otherSwipe = 1;
	}
}

function resetSlidePos() {

	TweenMax.to("#swipe_"+otherSwipe, 0, {x:0, y:0, overwrite:true});
	$("#swipe_"+otherSwipe).children(".statement").css( "opacity", 0 );
	$("#swipe_"+otherSwipe).children(".statement").children(".number").css( "opacity", 0 );
	$("#swipe_"+otherSwipe).children(".statement").children(".separator").css( "opacity", 0 );
	$("#swipe_"+otherSwipe).children(".statement").children(".text").css( "opacity", 0 );

	$("#swipe_"+otherSwipe).css( "zIndex", 0 );
	$("#swipe_"+currentSwipe).css( "zIndex", 10 );

}

function shuffle(a) {
	var j, x, i;
	for (i = a.length; i; i--) {
		j = Math.floor(Math.random() * i);
		x = a[i - 1];
		a[i - 1] = a[j];
		a[j] = x;
	}
}