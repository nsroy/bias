function initIcons() {
	TweenMax.to($("#icon_racism"), 0, {scale:.5, transformOrigin:"94px 94px"});
	TweenMax.to($("#icon_sexism"), 0, {scale:.5, transformOrigin:"94px 94px"});
	TweenMax.to($("#icon_homophobia"), 0, {scale:.5, transformOrigin:"94px 94px"});
	TweenMax.to($("#icon_ageism"), 0, {scale:.5, transformOrigin:"94px 94px"});
	TweenMax.to($("#icon_religion"), 0, {scale:.5, transformOrigin:"94px 94px"});
	TweenMax.to($("#icon_weight"), 0, {scale:.5, transformOrigin:"94px 94px"});
	TweenMax.to($("#icon_handicap"), 0, {scale:.5, transformOrigin:"94px 94px"});

	//animateHandicap();
	console.log("init icons")
}

function loadPicto(_picto, _delay) {
	if(_delay == null) {
		_delay = 1
	}
	hideOthers()
	$("#icon_"+_picto).css( "zIndex", 25 );
	//$("#icon_"+_picto).css( "marginTop", -75 );
	
	TweenMax.to($("#icon_"+_picto), 0, {y:-30, ease:Quad.easeOut, delay:0});
	
	
	if(_picto == "racism") {
		animateRacism(_delay);
	} else  if(_picto == "sexism") {
		animateSexism(_delay);
	} else  if(_picto == "homophobia") {
		animateHomophobia(_delay);
	} else  if(_picto == "ageism") {
		animateAgeism(_delay);
	} else  if(_picto == "weight") {
		animateWeight(_delay);
	} else  if(_picto == "religion") {
		animateReligion(_delay);
	} else  if(_picto == "weapons") {
		animateWeapons(_delay);
	} else  if(_picto == "handicap") {
		animateHandicap(_delay);
	}      
}


function hideOthers() {
	TweenMax.to($("#icon_homophobia"), 0, {alpha:0, ease:Quad.easeOut, delay:0});
	TweenMax.to($("#icon_sexism"), 0, {alpha:0, ease:Quad.easeOut, delay:0});
	TweenMax.to($("#icon_racism"), 0, {alpha:0, ease:Quad.easeOut, delay:0});

	TweenMax.to($("#icon_ageism"), 0, {alpha:0, ease:Quad.easeOut, delay:0});
	TweenMax.to($("#icon_weight"), 0, {alpha:0, ease:Quad.easeOut, delay:0});
	TweenMax.to($("#icon_religion"), 0, {alpha:0, ease:Quad.easeOut, delay:0});
	TweenMax.to($("#icon_handicap"), 0, {alpha:0, ease:Quad.easeOut, delay:0});
	TweenMax.to($("#icon_weapons"), 0, {alpha:0, ease:Quad.easeOut, delay:0});

}

function animateRacism(_delay) {
	if(_delay == null) {
		_delay = 1
	}
	var delay = _delay;
	TweenMax.to($("#icon_racism"), _delay, {alpha:1, ease:Quad.easeOut, delay:delay});
	//delay+=1;
	TweenMax.to($("#icon_racism_circle_white"), 1.3, {x:+150, ease:Expo.easeIn, delay:delay});
	TweenMax.to($("#icon_racism_circle_empty"), 1.3, {x:+150, ease:Expo.easeIn, delay:delay});
	TweenMax.to($("#icon_racism_circle_black"), 1.3, {x:-150, ease:Expo.easeIn, delay:delay});
	delay+=1.3;
	TweenMax.to($("#icon_racism_circle_white"), 1.5, {x:-40, ease:Expo.easeOut, delay:delay});
	TweenMax.to($("#icon_racism_circle_empty"), 1.5, {x:-40, ease:Expo.easeOut, delay:delay});
	TweenMax.to($("#icon_racism_circle_black"), 1.5, {x:+40, ease:Expo.easeOut, delay:delay, onComplete:iconDone, onCompleteParams:["racism"]});
	delay+=3;

	/*TweenMax.to($("#icon_racism_circle_white"), .8, {x:0, ease:Expo.easeInOut, delay:delay});
	TweenMax.to($("#icon_racism_circle_empty"), .8, {x:0, ease:Expo.easeInOut, delay:delay});
	TweenMax.to($("#icon_racism_circle_black"), .8, {x:0, ease:Expo.easeInOut, delay:delay});
	delay+=.5
	TweenMax.to($(".type"), .5, {alpha:0, ease:Quad.easeOut, delay:delay, overwrite:true});
	delay+=.2
	TweenMax.to($("#icon_racism"), .5, {alpha:0, ease:Quad.easeOut, delay:delay});
	delay+=.2
	TweenMax.to($(".introtext"), .5, {alpha:0, ease:Quad.easeOut, delay:delay});
	*/

}
function animateSexism(_delay) {
	if(_delay == null) {
		_delay = 1
	}
	var delay = _delay;
	TweenMax.to($(".male"), 0, {rotation:45, delay:0, transformOrigin:"left bottom"});
	TweenMax.to($("#icon_sexism"), _delay, {alpha:1, ease:Quad.easeOut, delay:delay});
	//delay+=1;
	TweenMax.to($("#icon_sexism_circle_empty_1"), 1, {x:+150, ease:Expo.easeIn, delay:delay});
	TweenMax.to($("#icon_sexism_circle_empty_2"), 1, {x:-150, ease:Expo.easeIn, delay:delay});
	delay+=1;
	TweenMax.to($("#icon_sexism_circle_empty_1"), .8, {x:-40, ease:Expo.easeOut, delay:delay});
	TweenMax.to($("#icon_sexism_circle_empty_2"), .8, {x:+40, ease:Expo.easeOut, delay:delay});
	delay+=1;
	TweenMax.to($(".female"), .5, {top:178, ease:Expo.easeOut, delay:delay});
	//TweenMax.to($(".male"), .6, {rotation:0, ease:Quad.easeOut, delay:delay});
	delay+=.4
	TweenMax.to($(".male"),.6, {rotation:0, left:150, top:-5, ease:Elastic.easeOut, delay:delay, transformOrigin:"left bottom", onComplete:iconDone, onCompleteParams:["sexism"]});
	delay+=3;
	/*TweenMax.to($(".male"), .4, {left:110, top:44,delay:delay, ease:Back.easeIn, transformOrigin:"left bottom"});
	TweenMax.to($(".female"), .4, {top:127, ease:Expo.easeIn, delay:delay});
	delay+=.4
	TweenMax.to($("#icon_sexism_circle_empty_1"), .8, {x:0, ease:Expo.easeInOut, delay:delay});
	TweenMax.to($("#icon_sexism_circle_empty_2"), .8, {x:0, ease:Expo.easeInOut, delay:delay});
	delay+=.8
	TweenMax.to($("#icon_sexism"), .5, {alpha:0, ease:Quad.easeOut, delay:delay});*/
}

function animateHomophobia(_delay) {
	if(_delay == null) {
		_delay = 1
	}
	var delay = _delay;
	TweenMax.to($(".male1"), 0, {rotation:90, delay:0, transformOrigin:"left bottom"});

	TweenMax.to($(".male2"), 0, {rotation:45, delay:0, transformOrigin:"left bottom"});
	TweenMax.to($("#icon_homophobia"), _delay, {alpha:1, ease:Quad.easeOut, delay:delay});
	//delay+=1;
	TweenMax.to($("#icon_homophobia_circle_empty_1"), 1, {x:+150, ease:Expo.easeIn, delay:delay});
	TweenMax.to($("#icon_homophobia_circle_empty_2"), 1, {x:-150, ease:Expo.easeIn, delay:delay});
	delay+=1;
	TweenMax.to($("#icon_homophobia_circle_empty_1"), .8, {x:-40, ease:Expo.easeOut, delay:delay});
	TweenMax.to($("#icon_homophobia_circle_empty_2"), .8, {x:+40, ease:Expo.easeOut, delay:delay});
	delay+=1;
	TweenMax.to($(".male1"), .6, {rotation:135, top:110, ease:Elastic.easeOut, delay:delay});
	//TweenMax.to($(".male"), .6, {rotation:0, ease:Quad.easeOut, delay:delay});
	//delay+=.4
	TweenMax.to($(".male2"),.6, {rotation:0, left:150, top:-5, ease:Elastic.easeOut, delay:delay, transformOrigin:"left bottom", onComplete:iconDone, onCompleteParams:["homophobia"]});
	delay+=3
	/*TweenMax.to($(".male2"), .4, {left:110, top:44,delay:delay, ease:Back.easeIn, transformOrigin:"left bottom"});
	TweenMax.to($(".male1"), .4, {top:50, ease:Back.easeIn, delay:delay});
	delay+=.4
	TweenMax.to($("#icon_homophobia_circle_empty_1"), .8, {x:0, ease:Expo.easeInOut, delay:delay});
	TweenMax.to($("#icon_homophobia_circle_empty_2"), .8, {x:0, ease:Expo.easeInOut, delay:delay});
	delay+=.8
	TweenMax.to($("#icon_homophobia"), .5, {alpha:0, ease:Quad.easeOut, delay:delay});*/
}

function animateAgeism(_delay) {
	if(_delay == null) {
		_delay = 1
	}
	var delay = _delay;
	TweenMax.killTweensOf($("#icon_ageism_hour"));
	TweenMax.killTweensOf($("#icon_ageism_arrow"));
	TweenMax.to($("#icon_ageism_circle"), 0, {scale:.2, transformOrigin:"13px 13px"});
	TweenMax.to($("#icon_ageism_arrow"), 0, {scaleX:0,delay:0, transformOrigin:"0px 14px"});
	TweenMax.to($("#icon_ageism_arrow"), 0, {scaleX:0, rotation:0, delay:0, transformOrigin:"0px 14px", overwrite:true});
	TweenMax.to($("#icon_ageism_hour"), 0, {scaleX:0, rotation:-135, delay:0, transformOrigin:"0px 14px", overwrite:true});


	TweenMax.to($("#icon_ageism"), _delay, {alpha:1, ease:Quad.easeOut, delay:delay});
	//delay+=1;
	TweenMax.to($("#icon_ageism_circle_white"), 1, {x:-10, y:-10, ease:Expo.easeInOut, delay:delay});
	TweenMax.to($("#icon_ageism_circle_black"), 1, {x:+10, y:+10, ease:Expo.easeInOut, delay:delay});
	delay+=1;
	TweenMax.to($("#icon_ageism_circle"), .1, {alpha:1, ease:Quad.easeOut, delay:delay,transformOrigin:"13px 13px"});

	TweenMax.to($("#icon_ageism_circle"), .4, {scale:1,  ease:Back.easeOut, delay:delay,transformOrigin:"13px 13px"});
	delay+=.5;
	TweenMax.to($("#icon_ageism_hour"), .2, {alpha:1, ease:Quad.easeOut, delay:delay});
	TweenMax.to($("#icon_ageism_hour"), .3, {scaleX:1, ease:Back.easeOut, delay:delay, transformOrigin:"0px 14px"});
	delay+=.1
	TweenMax.to($("#icon_ageism_arrow"), .3, {alpha:1, ease:Quad.easeOut, delay:delay});
	TweenMax.to($("#icon_ageism_arrow"), .3, {scaleX:1, ease:Back.easeOut, delay:delay, transformOrigin:"0px 14px",onComplete:iconDone, onCompleteParams:["ageism"]});
	delay+=.1
	TweenMax.to($("#icon_ageism_hour"), 24, {rotation:-135+360, repeat:-1, ease:Linear.easeNone, delay:delay, transformOrigin:"0px 14px"});
	TweenMax.to($("#icon_ageism_arrow"), 2, {rotation:360, repeat:-1, ease:Linear.easeNone, delay:delay, transformOrigin:"0px 14px"});
	delay+=3;
	/*TweenMax.to($("#icon_ageism_hour"), .3, {alpha:0, ease:Back.easeIn, delay:delay});
	TweenMax.to($("#icon_ageism_hour"), .3, {scaleX:0, ease:Back.easeIn, delay:delay, transformOrigin:"0px 14px"});
	TweenMax.to($("#icon_ageism_arrow"), .3, {alpha:0, ease:Back.easeIn, delay:delay});
	TweenMax.to($("#icon_ageism_arrow"), .3, {scaleX:0, ease:Back.easeIn, delay:delay, transformOrigin:"0px 14px"});
	delay+=.2
	TweenMax.to($("#icon_ageism_circle"), .3, {alpha:0,scale:0,  ease:Back.easeIn, delay:delay,transformOrigin:"13px 13px"});
	delay+=.3
	TweenMax.to($("#icon_ageism_circle_white"), .8, {x:0, y:0, ease:Expo.easeInOut, delay:delay});
	TweenMax.to($("#icon_ageism_circle_black"), .8, {x:0, y:0, ease:Expo.easeInOut, delay:delay});
	delay+=.8
	TweenMax.to($("#icon_ageism"), .5, {alpha:0, ease:Quad.easeOut, delay:delay});*/

//	TweenMax.to($("#icon_ageism_circle_white"), .8, {x:-40, ease:Expo.easeOut, delay:delay});
	//TweenMax.to($("#icon_ageism_circle_black"), .8, {x:+40, ease:Expo.easeOut, delay:delay});
}

function animateWeight(_delay) {
	if(_delay == null) {
		_delay = 1
	}
	var delay = _delay;
	TweenMax.killTweensOf($("#icon_weight_hour"));
	TweenMax.to($("#icon_weight_circle"), 0, {scale:.2, transformOrigin:"13px 13px"});
	TweenMax.to($("#icon_weight_hour"), 0, {scaleX:0, rotation:-135, delay:0, transformOrigin:"0px 14px", overwrite:true});

	TweenMax.to($("#icon_weight_scale"), 0, {y:+100, delay:0});

	TweenMax.to($("#icon_weight"), _delay, {alpha:1, ease:Quad.easeOut, delay:delay});
	//delay+=1;
	delay+=1;
	TweenMax.to($("#icon_weight_scale"), .1, {alpha:1,  delay:delay, ease:Quad.easeOut});

	TweenMax.to($("#icon_weight_scale"), .4, { y:-20, delay:delay, ease:Back.easeOut});

	TweenMax.to($("#icon_weight_circle"), .1, {alpha:1, ease:Quad.easeOut, delay:delay,transformOrigin:"13px 13px"});

	TweenMax.to($("#icon_weight_circle"), .4, {scale:1,  ease:Back.easeOut, delay:delay,transformOrigin:"13px 13px"});
	delay+=.1;
	TweenMax.to($("#icon_weight_hour"), .2, {alpha:1, ease:Quad.easeOut, delay:delay});
	TweenMax.to($("#icon_weight_hour"), .3, {scaleX:1, ease:Back.easeOut, delay:delay, transformOrigin:"0px 14px", onComplete:iconDone, onCompleteParams:["weight"]});
	delay+=.1
	TweenMax.to($("#icon_weight_hour"), 1, {rotation:-135+120,  ease:Expo.easeIn, delay:delay, transformOrigin:"0px 14px"});
	TweenMax.to($("#icon_weight_hour"), .05, {rotation:-135+125, repeat:-1, yoyo:true, ease:Linear.easeNone, delay:delay+1, transformOrigin:"0px 14px"});
	delay+=4;
	/*TweenMax.to($("#icon_weight_hour"), .3, {alpha:0, ease:Back.easeIn, delay:delay});
	TweenMax.to($("#icon_weight_hour"), .3, {scaleX:0, ease:Back.easeIn, delay:delay, transformOrigin:"0px 14px"});
	delay+=.2
	TweenMax.to($("#icon_weight_circle"), .3, {alpha:0,scale:0,  ease:Back.easeIn, delay:delay,transformOrigin:"13px 13px"});
	TweenMax.to($("#icon_weight_scale"), .3, {alpha:0, y:+100, delay:delay, ease:Back.easeIn});

	delay+=.3
	TweenMax.to($("#icon_weight_circle_black"), .8, {x:0, y:0, ease:Expo.easeInOut, delay:delay});
	delay+=.8
	TweenMax.to($("#icon_weight"), .5, {alpha:0, ease:Quad.easeOut, delay:delay});*/

//	TweenMax.to($("#icon_ageism_circle_white"), .8, {x:-40, ease:Expo.easeOut, delay:delay});
	//TweenMax.to($("#icon_ageism_circle_black"), .8, {x:+40, ease:Expo.easeOut, delay:delay});
}

function animateReligion(_delay) {
	if(_delay == null) {
		_delay = 1
	}
	var delay = _delay;
	TweenMax.to($("#icon_religion_cross"), 0, {scale:.2, transformOrigin:"30px 58px"});

	TweenMax.to($("#icon_religion"), _delay, {alpha:1, ease:Quad.easeOut, delay:delay, overwrite:true});
	delay+=.8;
	TweenMax.to($("#icon_religion_cross"), 2, {alpha:1, scale:1, delay:delay, ease:Back.easeOut, transformOrigin:"30px 58px"});
	delay+=.3;
	TweenMax.to($("#icon_religion_halo"), .5, {alpha:1, ease:Quad.easeOut, delay:delay});
	TweenMax.to($("#icon_religion_halo"), 2, {y:60,  ease:Back.easeOut, delay:delay, onComplete:iconDone, onCompleteParams:["religion"]});
	delay+=3;
	/*TweenMax.to($("#icon_religion_halo"), .3, {alpha:0, ease:Quad.easeOut, delay:delay+.7});
	TweenMax.to($("#icon_religion_halo"), 1, {y:0,  ease:Back.easeIn, delay:delay});
	TweenMax.to($("#icon_religion_cross"), 1, {alpha:0, scale:.2, delay:delay, ease:Back.easeIn, transformOrigin:"30px 58px"});
	delay+=1
	TweenMax.to($("#icon_religion"), .5, {alpha:0, ease:Quad.easeOut, delay:delay});*/


}

function animateWeapons(_delay) {
	if(_delay == null) {
		_delay = 1
	}
	var delay = _delay;
	TweenMax.to($("#icon_weapons"), _delay, {alpha:1, ease:Quad.easeOut, delay:delay});
	delay+=.5
	TweenMax.to($("#icon_weapons_trigger"), .5, {alpha:1, ease:Quad.easeOut, delay:delay, onComplete:iconDone, onCompleteParams:["weapons"]});
	
}

function animateHandicap(_delay) {
	if(_delay == null) {
		_delay = 1
	}
	var delay = _delay;
	TweenMax.to($("#icon_handicap_bar1"), 0, {alpha:0, scaleX:0, rotation:-115, delay:0, transformOrigin:"0px 14px", overwrite:true});
	TweenMax.to($("#icon_handicap_bar2"), 0, {alpha:0, scaleX:0, delay:0, transformOrigin:"0px 14px", overwrite:true});

	TweenMax.to($("#icon_handicap"), _delay, {alpha:1, ease:Quad.easeOut, delay:delay, overwrite:true});
	delay+=.8;
	TweenMax.to($("#icon_handicap_wheel"), 1, {alpha:1, ease:Quad.easeOut, delay:delay, overwrite:true});
	TweenMax.to($("#icon_handicap_wheel"), 5, {rotation:360, repeat:-1, delay:delay, ease:Linear.easeNone, transformOrigin:"94px 94px"});

	delay+=.5
	TweenMax.to($("#icon_handicap_bar1"), .2, {alpha:1, ease:Quad.easeOut, delay:delay});
	TweenMax.to($("#icon_handicap_bar2"), .2, {alpha:1, ease:Quad.easeOut, delay:delay});
	TweenMax.to($("#icon_handicap_bar1"), .3, {scaleX:1, ease:Back.easeOut, delay:delay, transformOrigin:"0px 14px"});
	TweenMax.to($("#icon_handicap_bar2"), .3, {scaleX:1, ease:Back.easeOut, delay:delay, transformOrigin:"0px 14px", onComplete:iconDone, onCompleteParams:["handicap"]});


	delay+=3;
/*
	TweenMax.to($("#icon_handicap_bar1"), .3, {alpha:0,scaleX:0, ease:Back.easeIn, delay:delay});
	TweenMax.to($("#icon_handicap_bar2"), .3, {alpha:0,scaleX:0, ease:Back.easeIn, delay:delay});
	delay+=.3
	TweenMax.to($("#icon_handicap_wheel"), .3, {alpha:0, ease:Quad.easeOut, delay:delay});
	delay+=.3
	TweenMax.to($("#icon_handicap"), .5, {alpha:0, ease:Quad.easeOut, delay:delay});*/

}