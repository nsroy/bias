class Framework {
  	constructor(options) {
    	this.config = JSON.parse(options);
      this.hidden = false;
      // this._initEvent();
   	}
  	
    //xiti  - analytics stat
  	track(pageName, isClick) {
  		
      //xiti
      if(isClick) {
        ATTag.click.send({
          level2: '10',
          chapter1: "Webprod",
          chapter2:"60Secondes",
          chapter3: this.config.projectName, //facultatif
          name: pageName,
          type: "action",
          customVars: {
            site: {
              1 : this.config.language,
              3 : encodeURIComponent(document.location.href)
            }
          },
          customObject: {
            device : (function(){var bp = {s:600,d:1000};var wojd="error";if (typeof window.innerWidth!='undefined'){var w=window.innerWidth;if(w<bp.s){wojd="smartphone";}else if(w<bp.d) {wojd="tablet";}else if(w>=bp.d){wojd="desktop";}else{wojd="error";}};return wojd;})()
          }
        })
      }
      else{
        ATTag.page.send({
          level2: '10',
          chapter1: "Webprod",
          chapter2:"60Secondes",
          chapter3: this.config.projectName, //facultatif
          name: pageName, //obligatoire
          customVars: {
            site: {
              1 : this.config.language,
              3 : encodeURIComponent(document.location.href)
            }
          },
          customObject: {
            device : (function(){var bp = {s:600,d:1000};var wojd="error";if (typeof window.innerWidth!='undefined'){var w=window.innerWidth;if(w<bp.s){wojd="smartphone";}else if(w<bp.d) {wojd="tablet";}else if(w>=bp.d){wojd="desktop";}else{wojd="error";}};return wojd;})()
          }
        });
      }

      //analytics
  		for(var i=0;i<this.config.analytics.length;i++) {	
        ga(this.config.analytics[i].name + '.send', {
          hitType: 'event',
          eventCategory: pageName,
          eventAction: pageName,
          eventLabel: '/interactive/theenemy'
        });
      }
  	}


    _initEvent() {

      $("body").on("touchmove", (function(e){
        
        if(!this.hidden) {

          $(".hh_onf").addClass("hh_hidden")
            .on("animationend", function(e){
              $(e.target).hide();
            });

          this.hidden = true;
        }

      }).bind(this));
    }
}
